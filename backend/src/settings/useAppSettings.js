const useAppSettings = () => {
  let settings = {};
  settings['electricsoft_app_email'] = 'electricsoft_bugambilias@outlook.com';
  settings['electricsoft_base_url'] = 'http://localhost:3000/';
  return settings;
};

module.exports = useAppSettings;
