const useDate = ({ settings }) => {
  const { electricsoft_app_date_format } = settings;

  const dateFormat = electricsoft_app_date_format;

  return {
    dateFormat,
  };
};

module.exports = useDate;
