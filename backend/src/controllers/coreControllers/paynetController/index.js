const fetch = require("node-fetch");
require('dotenv').config();

const loginNetPay = async()=>{
    
        const username = 'trusted-app';
        const password = 'secret';
        const login64 = btoa(`${username}:${password}`)
        const params = new URLSearchParams();
        params.append('Auth_string', `${btoa(`Basic ${login64}`)}`);
        params.append('grant_type', 'password');
        params.append('username', 'Nacional');
        params.append('password', 'netpay');

        try {
        const result = await fetch(`${process.env.URL_TOKEN}`, { 
            method: 'POST',
            headers: {
                'Authorization': `Basic ${login64}`               
            },
            body:params
        }).then(res=>res.json());
        return result
        } catch (error) {
         console.log( error)
        }

}

const revisa_token = async()=>{
    const {access_token,expires_in} = await loginNetPay()

    return {access_token}
}

const venta = async(json)=>{
    // console.log(json)
    const {access_token} = await revisa_token()
    
    try {
        const result = await fetch(`${process.env.URL_SALE}`, { 
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                    "traceability": json,
                    "serialNumber": json.serialNumber,
                    "amount":json.total,
                    "folioNumber":json.folio,
                    "storeId": "9194",
                  //   "msi":"03",
                    "disablePrintAnimation": false
                })
        })
        .then(res=>res.json());
        
        return result

        } catch (error) {
            console.log( error )
        }
        
    
}

const reprint = async(serialNumber,orderId)=>{
    // console.log(json)
    const {access_token} = await revisa_token()
    
    try {
        const result = await fetch(`${process.env.URL_REPRINT}`, { 
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                    "traceability": {},
                    "serialNumber": serialNumber,
                    "orderId": orderId,
                    "storeId": "9194",
                    "disablePrintAnimation": false
                })
        })
        .then(res=>res.json());
        
        return result

        } catch (error) {
            console.log( error )
        }
        
    
}

const cancel = async(serialNumber,orderId)=>{
    // console.log(json)
    const {access_token} = await revisa_token()
    
    try {
        const result = await fetch(`${process.env.URL_CANCEL}`, { 
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${access_token}`,
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                    "traceability": {},
                    "serialNumber": serialNumber,
                    "orderId": orderId,
                    "storeId": "9194",
                    "disablePrintAnimation": false
                })
        })
        .then(res=>res.json());
        
        return result

        } catch (error) {
            console.log( error )
        }
        
    
}

module.exports = { venta , reprint, cancel}