const paginatedList = async (Model, req, res) => {

  console.log('entra aca paginatedList general ' )

  const page = req.query.page || 1;
  const limit = parseInt(req.query.items) || 10;
  const skip = page * limit - limit;

  const { sortBy = 'enabled', sortValue = -1, filter, equal } = req.query;

  const fieldsArray = req.query.fields ? req.query.fields.split(',') : [];

  let fields;
  const queryString = req.query.q;

  fields = fieldsArray.length === 0 ? {} : { $or: [] };

  // for (const field of fieldsArray) {
  //   fields.$or.push({ [field]: { $regex: new RegExp(req.query.q, 'i') } });
  // }

  if (fieldsArray.length > 0 && queryString) {
    fields = { $or: [] };
    for (const field of fieldsArray) {
      // console.log(field)
      // Solo usa $regex si el campo no es _id
      if (field !== '_id' && field !== 'company'  ) {
        // console.log('field',field, queryString)
        fields.$or.push({ [field]: { $regex: new RegExp(queryString, 'i') } });
      } else {
        // Maneja el caso para _id de forma directa
        fields.$or.push({ [field]: queryString });
      }
    }
  } else {
    fields = {};
  }

  console.log('filter',{[filter]: equal})
  console.log('fields',fields)

  //  Query the database for a list of all results
  const resultsPromise = Model.find({
    removed: false,

    [filter]: equal,
    ...fields,
  })
    .skip(skip)
    .limit(limit)
    .sort({ [sortBy]: sortValue })
    .populate()
    .exec();

  // Counting the total documents
  const countPromise = Model.countDocuments({
    removed: false,
    [filter]: equal,
    ...fields,
  });
  // Resolving both promises
  const [result, count] = await Promise.all([resultsPromise, countPromise]);

  // Calculating total pages
  const pages = Math.ceil(count / limit);

  // Getting Pagination Object
  const pagination = { page, pages, count };
  if (count > 0) {
    return res.status(200).json({
      success: true,
      result,
      pagination,
      message: 'Se encontraron todos los documentos con éxito.',
    });
  } else {
    return res.status(203).json({
      success: true,
      result: [],
      pagination,
      message: 'La colección está vacía',
    });
  }
};

module.exports = paginatedList;
