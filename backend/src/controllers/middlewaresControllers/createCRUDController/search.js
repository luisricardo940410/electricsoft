const search = async (Model, req, res) => {

  // console.log(req.query.fields)
  // if (req.query.q === undefined || req.query.q.trim() === '') {
  //   return res
  //     .status(202)
  //     .json({
  //       success: false,
  //       result: [],
  //       message: 'No document found by this request',
  //     })
  //     .end();
  // }


  const fieldsArray = req.query.fields ? req.query.fields.split(',') : ['name'];
  const queryArray = req.query.q ? req.query.q.split(','): [];
 
  let fields = { $or: [] };


  if (fieldsArray.length > 0 && queryArray.length > 0) {
    for (let i = 0; i < fieldsArray.length; i++) {
      const field = fieldsArray[i];
      const queryValue = queryArray[i] !== undefined ? queryArray[i] : '';
  
      // Solo agrega el campo a la consulta si queryValue no está vacío
      if (queryValue !== '') {
        if (field !== '_id' && field !== 'company') {
          // Usa $regex para búsquedas parciales
          fields.$or.push({ [field]: { $regex: new RegExp(queryValue, 'i') } });
        } else {
          // Maneja el caso para _id y company directamente
          fields.$or.push({ [field]: queryValue });
        }
      }
    }
  }else{
    fields = {};
  }
  

  console.log(fields)

  let results = await Model.find({
    ...fields,
  })

    .where('removed', false)
    .limit(20)
    .exec();

  if (results.length >= 1) {
    return res.status(200).json({
      success: true,
      result: results,
      message: 'Successfully found all documents',
    });
  } else {
    return res
      .status(202)
      .json({
        success: false,
        result: [],
        message: 'No document found by this request',
      })
      .end();
  }
};

module.exports = search;
