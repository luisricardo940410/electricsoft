const { emailVerfication, passwordVerfication } = require('@/emailTemplate/emailVerfication');
const nodemailer = require('nodemailer');

const { Resend } = require('resend');

const sendMail = async ({
  email,
  name,
  link,
  electricsoft_app_email,
  subject = 'Verify your email | Electricsoft',
  type = 'emailVerfication',
  emailToken,
}) => {



  // const resend = new Resend(process.env.RESEND_API);

  // const { data } = await resend.emails.send({
  //   from: idurar_app_email,
  //   to: email,
  //   subject,
  //   html:
  //     type === 'emailVerfication'
  //       ? emailVerfication({ name, link, emailToken })
  //       : passwordVerfication({ name, link }),
  // });


  const transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    port: 587 , // Puerto para STARTTLS
    secure: false, // true para el puerto 465, false para otros puertos
    auth: {
    user: electricsoft_app_email,
    pass: '$electrisoft.2024',
    },
  });

  const mailOptions = {
    from: `"Electicsoft" <${electricsoft_app_email}>`,
    to: email,
    subject: subject,
    html:
    type === 'emailVerfication'
      ? emailVerfication({ name, link, emailToken })
      : passwordVerfication({ name, link }),
  };

try{

  const info = await transporter.sendMail(mailOptions);
  const { messageId,response } = info;
  console.log('enviado',{ messageId:messageId, response: response })
  return { messageId:messageId, response: response };

}catch (error) {
  console.error('Error al enviar el correo:', error);
  throw error; // Relanzar el error para que pueda ser capturado fuera de la función
}
};

module.exports = sendMail;
