const fs = require('fs');
const custom = require('@/controllers/pdfController');
const { SendOffer } = require('@/emailTemplate/SendEmailTemplate');
const mongoose = require('mongoose');
const OfferModel = mongoose.model('Offer');
const { Resend } = require('resend');
const { loadSettings } = require('@/middlewares/settings');
const { useAppSettings } = require('@/settings');
const nodemailer = require('nodemailer');

const mail = async (req, res) => {
  const { id } = req.body;

  // Throw error if no id
  if (!id) {
    throw { name: 'ValidationError' };
  }

  const result = await OfferModel.findOne({
    _id: id,
    removed: false,
  }).exec();

  // Throw error if no result
  if (!result) {
    throw { name: 'ValidationError' };
  }

  // Continue process if result is returned
  const { lead } = result;
  const { name } = lead;
  const email = lead[lead.type].email;


  if (!email) {
    return res.status(403).json({
      success: false,
      result: null,
      message: 'El cliente potencial no tiene correo electrónico, agregue un nuevo correo electrónico e inténtelo nuevamente',
    });
  }

  const modelName = 'Offer';

  const fileId = modelName.toLowerCase() + '-' + result._id + '.pdf';
  const folderPath = modelName.toLowerCase();
  const targetLocation = `src/public/download/${folderPath}/${fileId}`;
  

  await custom.generatePdf(
    modelName,
    { filename: folderPath, format: 'A4', targetLocation:targetLocation },
    result,
    async () => {
       const {response} = await sendMail(
        email,
        name,
        targetLocation,
      );

      if (response) {
        OfferModel.findByIdAndUpdate({ _id: id, removed: false }, { status: 'sent' })
          .exec()
          .then((data) => {
            // Returning successfull response
            return res.status(200).json({
              success: true,
              result: response,
              message: `Remicion enviada correctamente a ${email}`,
            });
          });
      }
    }
  );
};


const sendMail = async (email, name, targetLocation) =>{
  // Configuración del transporter para SMTP con puertos
  // const transporter = nodemailer.createTransport({
  //     host: 'mail.electricabugambilias.com',
  //     port: 465, // Puerto para STARTTLS
  //     secure: true, // true para el puerto 465, false para otros puertos
  //     auth: {
  //     user: 'reportes_electrisoft@electricabugambilias.com',
  //     pass: 'bugambilias2024',
  //     method: 'PLAIN', 
  //     },
  //     tls: {
  //         rejectUnauthorized: true // Activar la verificación del certificado TLS
  //     },
  //     sent: true
  // });


  
  const transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    port: 587 , // Puerto para STARTTLS
    secure: false, // true para el puerto 465, false para otros puertos
    auth: {
    user: 'electricsoft_bugambilias@outlook.com',
    pass: '$electrisoft.2024',
    },
});

  const settings = await loadSettings();
  const electricsoft_app_email = 'reportes_electrisoft@electricabugambilias.com';
  const electricsoft_app_company_email = settings['electricsoft_app_company_email'];
  const company_name = settings['company_name'];
  console.log('company_name',company_name);
  console.log('electricsoft_app_company_email',electricsoft_app_company_email);

  // Read the file to be attatched
  // const attatchedFile = fs.readFileSync(targetLocation);

  //$electrisoft.2024
  // Detalles del correo electrónico  compras@electricabugambilias.com
  const mailOptions = {
      from: '"Electicsoft" <electricsoft_bugambilias@outlook.com>',
      to: 'webmaster@logom.com.mx',
      cc:'luisricardo940410@gmail.com',
      bcc: 'lcruz@motidigital.com', // Dirección de correo para copia oculta
      subject: 'Remicion de ',
      attachments: [
        {
          filename: 'Remicion.pdf',
          path: targetLocation,
        },
      ],
      html: SendOffer({ name, title: 'Remicion Electrisoft' }) ,
  };


  // SendOffer({ name, title: 'Remicion Electrisoft' })
  try{
    // Verificar la conexión con el servidor SMTP
    // transporter.verify((error, success) => {
    //   if (error) {
    //       console.error('Error al verificar la conexión:', error);
    //   } else {
    //       console.log('Conexión exitosa con el servidor SMTP',success);
    //   }
    // });
    const info = await transporter.sendMail(mailOptions);
    const { messageId,response } = info;
    return { messageId:messageId, response: response };

  }catch (error) {
    console.error('Error al enviar el correo:', error);
    throw error; // Relanzar el error para que pueda ser capturado fuera de la función
  }
} 


module.exports = mail;
