const custom = require('@/controllers/pdfController');
const { SendSale } = require('@/emailTemplate/SendEmailTemplate');
const mongoose = require('mongoose');
const SaleModel = mongoose.model('Sale');
const { loadSettings } = require('@/middlewares/settings');
const { useAppSettings } = require('@/settings');
const nodemailer = require('nodemailer');

const mail = async (req, res) => {
  const { id } = req.body;

  // Throw error if no id
  if (!id) {
    throw { name: 'ValidationError' };
  }

  const result = await SaleModel.findOne({
    _id: id,
    removed: false,
  }).exec();

  // Throw error if no result

  
  if (!result) {
    throw { name: 'ValidationError' };
  }

  // Continue process if result is returned
  const { people } = result.client;
  const { firstname,lastname } = people;
  const email = people.email;
  console.log('email',email)

  if (!email) {
    return res.status(403).json({
      success: false,
      result: null,
      message: 'El cliente potencial no tiene correo electrónico, agregue un nuevo correo electrónico e inténtelo nuevamente',
    });
  }

  const modelName = 'Sale';

  const fileId = modelName.toLowerCase() + '-' + result._id + '.pdf';
  const folderPath = modelName.toLowerCase();
  const targetLocation = `src/public/download/${folderPath}/${fileId}`;
  

  await custom.generatePdf(
    modelName,
    { filename: folderPath, format: 'A4', targetLocation:targetLocation },
    result,
    async () => {
       const {response} = await sendMail(
        email,
        firstname+' '+lastname,
        targetLocation,
      );

      if (response) {
        SaleModel.findByIdAndUpdate({ _id: id, removed: false }, { status: 'sent' })
          .exec()
          .then((data) => {
            // Returning successfull response
            return res.status(200).json({
              success: true,
              result: response,
              message: `Venta enviada correctamente a ${email}`,
            });
          });
      }
    }
  );
};


const sendMail = async (email, name, targetLocation) =>{

  const transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    port: 587 , // Puerto para STARTTLS
    secure: false, // true para el puerto 465, false para otros puertos
    auth: {
    user: 'electricsoft_bugambilias@outlook.com',
    pass: '$electrisoft.2024',
    },
});

  const settings = await loadSettings();
  // const electricsoft_app_email = 'electricsoft_bugambilias@outlook.com';
  const electricsoft_app_company_email = settings['electricsoft_app_company_email'];
  const company_name = settings['company_name'];

  const mailOptions = {
      from: `"Electricsoft" <${electricsoft_app_company_email}>`,
      to: email,
      subject: 'Venta de '+company_name,
      attachments: [
        {
          filename: 'Venta.pdf',
          path: targetLocation,
        },
      ],
      html: SendSale({ name, title: 'Venta de '+ company_name}) ,
  };
  try{
    
    const info = await transporter.sendMail(mailOptions);
    const { messageId,response } = info;
    return { messageId:messageId, response: response };

  }catch (error) {
    console.error('Error al enviar el correo:', error);
    throw error; 
  }
} 


module.exports = mail;
