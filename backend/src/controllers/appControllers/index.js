const createCRUDController = require('@/controllers/middlewaresControllers/createCRUDController');
const { singleStorageUpload } = require('@/middlewares/uploadMiddleware');
const { routesList } = require('@/models/utils');

const { globSync } = require('glob');
const path = require('path');

const pattern = './src/controllers/appControllers/*/**/';
const controllerDirectories = globSync(pattern).map((filePath) => {
  return path.basename(filePath);
});

const appControllers = () => {
  const controllers = {};
  const hasCustomControllers = []; //efi con mongo 

  controllerDirectories.forEach((controllerName) => {
    try {
      const customController = require('@/controllers/appControllers/' + controllerName);

      if (customController) {

        hasCustomControllers.push(controllerName);   

        
      // Solo añade el middleware para companyController
      // if (controllerName === 'companyController') {
      //   if (customController.update && typeof customController.update === 'function') {
      //     const originalUpdate = customController.update;

      //     // Reemplazamos el método update por una nueva función
      //     customController.update = async (req, res, next) => {
      //       // Ejecuta el middleware
      //       await singleStorageUpload({ entity: 'company', fieldName: 'logo', fileType: 'image' })(req, res, async () => {
      //         console.log('Middleware ejecutado');
      //         try {
      //           // Llamar a la función original
                
      //           await originalUpdate(req, res);
      //         } catch (error) {
      //           console.error('Error en la función original:', error, next);
      //           next(error); // Pasa cualquier error al siguiente middleware
      //         }
      //       });
      //     };
      //   }
      // }

        controllers[controllerName] = customController;
        
      }
    } catch (err) {
      throw new Error(err.message);
    }
  });
  console.log('routesList',routesList)

  routesList.forEach(({ modelName, controllerName }) => {
    if (!hasCustomControllers.includes(controllerName)) {
      controllers[controllerName] = createCRUDController(modelName);
    }
  });

  return controllers;
};

module.exports = appControllers();
