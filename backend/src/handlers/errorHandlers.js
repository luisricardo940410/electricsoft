/*
Controlador de errores de captura

  Con async/await, necesitas alguna forma de detectar errores
  En lugar de usar try{} catch(e) {} en cada controlador, ajustamos la función en
  catchErrors(), detecta cualquier error que arroje y pásalo a nuestro middleware express con next()
*/

// exports.catchErrors = (fn) => {
//   return function (req, res, next) {
//     return fn(req, res, next).catch((error) => {

//       if (error.name == 'ValidationError') {
//         return res.status(400).json({
//           success: false,
//           result: null,
//           message: 'Required fields are not supplied',
//           controller: fn.name,
//           error: error,
//         });
//       } else {
//         // Server Error
//         return res.status(500).json({
//           success: false,
//           result: null,
//           message: error.message,
//           controller: fn.name,
//           error: error,
//         });
//       }
//     });
//   };
// };

exports.catchErrors = (fn) => {
  return function (req, res, next) {
    // console.log('Executing catchErrors for function:', fn.name);
    // Ejecutar la función fn y manejar cualquier error
    try {
      // Llamar a la función fn y obtener su resultado
      const result = fn(req, res, next);

      // Verificar si el resultado es una promesa
      if (result instanceof Promise) {
        // Si result es una promesa, retornarla y manejar errores con catch
        return result.catch((error) => {
          handleCatchError(error, req, res, fn);
        });
      } else {
        // Si result no es una promesa, devolver un Promise.reject para manejar errores
        return Promise.reject(new Error('Function does not return a promise'));
      }
    } catch (error) {
      // Manejar errores que ocurren durante la ejecución de fn
      handleCatchError(error, req, res, fn);
    }
  };
};

// Función para manejar errores en catchErrors
function handleCatchError(error, req, res, fn) {
  if (error.name == 'ValidationError') {
    return res.status(400).json({
      success: false,
      result: null,
      message: 'Required fields are not supplied',
      controller: fn.name,
      error: error,
    });
  } else {
    // Server Error
    return res.status(500).json({
      success: false,
      result: null,
      message: error.message,
      controller: fn.name,
      error: error,
    });
  }
}

/*
  Not Found Error Handler

  If we hit a route that is not found, we mark it as 404 and pass it along to the next error handler to display
*/
exports.notFound = (req, res, next) => {
  return res.status(404).json({
    success: false,
    message: "Api url doesn't exist ",
  });
};

/*
  Development Error Handler

  In development we show good error messages so if we hit a syntax error or any other previously un-handled error, we can show good info on what happened
*/
exports.developmentErrors = (error, req, res, next) => {
  error.stack = error.stack || '';
  const errorDetails = {
    message: error.message,
    status: error.status,
    stackHighlighted: error.stack.replace(/[a-z_-\d]+.js:\d+:\d+/gi, '<mark>$&</mark>'),
  };

  return res.status(500).json({
    success: false,
    message: error.message,
    error: error,
  });
};

/*
  Manejador de errores de producción

  No se filtran rastros de pila al administrador
*/
exports.productionErrors = (error, req, res, next) => {
  return res.status(500).json({
    success: false,
    message: error.message,
    error: error,
  });
};
