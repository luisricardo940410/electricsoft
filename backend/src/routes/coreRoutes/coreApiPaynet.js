const express = require('express');
const router = express.Router();
const payments = require('../../models/coreModels/SalesPayment');
const { venta, reprint, cancel } = require('../../controllers/coreControllers/paynetController');


router.post('/response', async(req, res) => {
    const payNetResponse = req.body
    const {orderId='',folioNumber='0',message='',amount=0} = payNetResponse 
    const pag = new payments({ orderId,folioNumber,message,amount,payNetResponse})
    await pag.save()

    return res.json({ok:1})
})

router.post('/check_pay', async(req, res) => {
  const {folio} = req.body 
  const pag = await payments.findOne({folioNumber:folio})

  if(pag){
    const {orderId,folioNumber,message,amount} = pag
    console.log(pag)
    return res.json({orderId,folioNumber,message,amount,error:''})
  }else{
    return res.json({error:'No encontrado el pago aun en bd'})
  }

})

router.post('/sale', async(req, res) => {
  const resp = await venta(req.body)
  return res.json(  resp )
})

router.post('/reprint', async(req, res) => {
  const {serialNumber,orderId} = req.body
  const resp = await reprint(serialNumber,orderId)
  return res.json(  resp )
})

router.post('/cancel', async(req, res) => {
  const {serialNumber,orderId} = req.body
  const resp = await cancel(serialNumber,orderId)
  return res.json(  resp )
})

module.exports = router;