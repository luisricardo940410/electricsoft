const express = require('express');
const { catchErrors } = require('@/handlers/errorHandlers');
const router = express.Router();

const { hasPermission } = require('@/middlewares/permission');
const appControllers = require('@/controllers/appControllers');
const { routesList } = require('@/models/utils');
const { singleStorageUpload } = require('@/middlewares/uploadMiddleware');

const routerApp = (entity, controller) => {

  router.route(`/${entity}/create`).post(
    hasPermission('create'),
    //singleStorageUpload({ entity: entity, fieldName: 'photo', fileType: 'image' }),
    catchErrors(controller['create'])
  );

  router.route(`/${entity}/read/:id`).get(hasPermission('read'), catchErrors(controller['read']));
   //  quit : fieldName:'photo'
  router
    .route(`/${entity}/update/:id`)
    .patch(
      hasPermission('update'),
      singleStorageUpload({ entity: entity, fileType: 'image' }),
      catchErrors(controller['update'])
    );

  router
    .route(`/${entity}/delete/:id`)
    .delete(hasPermission('delete'), catchErrors(controller['delete']));

  router.route(`/${entity}/search`).get(hasPermission('read'), catchErrors(controller['search']));
  router.route(`/${entity}/list`).get(hasPermission('read'), catchErrors(controller['list']));
  router.route(`/${entity}/listAll`).get(hasPermission('read'), catchErrors(controller['listAll']));
  router.route(`/${entity}/filter`).get(hasPermission('read'), catchErrors(controller['filter']));
  router.route(`/${entity}/summary`).get(hasPermission('read'), catchErrors(controller['summary']));

  if (entity === 'invoice' || entity === 'quote' || entity === 'offer'  || entity === 'sale' || entity === 'payment') {
    router.route(`/${entity}/mail`).post(hasPermission('update'), catchErrors(controller['mail']));
  }

  if (entity === 'quote') {
    router
      .route(`/${entity}/convert/:id`)
      .get(hasPermission('update'), catchErrors(controller['convert']));
  }

    // Imprimir las rutas generadas
    // router.stack.forEach((layer) => {
    //   if (layer.route) {
    //     const methods = Object.keys(layer.route.methods).map(method => method.toUpperCase()).join(', ');
    //     console.log(`${methods} - ${layer.route.path}`);
    //   }
    // });

    // router.stack.forEach((layer) => {
    //   if (layer.route) {
    //     const methods = Object.keys(layer.route.methods).map(method => method.toUpperCase()).join(', ');
    //     const path = layer.route.path;
    
    //     // Obtener el nombre de cada middleware
    //     const middlewareNames = layer.route.stack
    //       .map(middlewareLayer => middlewareLayer.name || 'Middleware anónimo')
    //       .join(', ');
    
    //     console.log(`${methods} - ${path} | Middlewares: ${middlewareNames}`);
    //   }
    // });
};

routesList.forEach(({ entity, controllerName }) => {
  const controller = appControllers[controllerName];
  routerApp(entity, controller);
});

module.exports = router;
