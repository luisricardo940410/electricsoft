const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const home = new Schema({
  removed: {
    type: Boolean,
    default: false,
  },
  enabled: {
    type: Boolean,
    default: true,
  },
  name: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  description: {
    type: String,
    uppercase: true,
    required: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

home.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Home', home);
