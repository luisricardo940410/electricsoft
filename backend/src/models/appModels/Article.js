const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const articuloSchema = new Schema({
    removed: {
      type: Boolean,
      default: false,
    },
    storeCode: {
      type: String,
      uppercase: true,
      trim: true,
      required: true,
    },
    provider: {
      type: mongoose.Schema.ObjectId,
      ref: 'Supplier',
      required: true,
      autopopulate: true,
    },
    providerCode: {
      type: String,
      uppercase: true,
      trim: true,
      // required: true,
    },
    description: {
        type: String,
        uppercase: true,
        required: true,
    },
    quantity: {
        type: Number,
        // required: true,
    },
    minimumQuantity: {
      type: Number,
      // required: true,
    },
    maximumQuantity: {
      type: Number,
      // required: true,
    },
    notes: {
      type: String,
    },
    lastExit: {
      type: Date,
    }, 
    lastEntry: {
      type: Date,
    },
    unitMeasurement : {
      type: mongoose.Schema.ObjectId,
      ref: 'UnitMeasurement',
      required: true,
      autopopulate: true,
    },
    priceList: {
        type: Number,

    },
    priceA: {
      type: Number,
    },
    priceB: {
      type: Number,
    },
    priceC: {
      type: Number,
    },
    priceD: {
      type: Number,
    },
    promotion: {
      type: Boolean,
    },
    pricePromotion: {
      type: Number,
    },
    state: {
        type: String,
        default: 'Activo',
    },
    photo: {
      type: String,

    },
    datasheet: {
      type: String,

    },
    createdAt: {
      type: Date,
      default: Date.now,
    },                                 
  });
  
  articuloSchema.plugin(require('mongoose-autopopulate'));
  module.exports = mongoose.model('Article', articuloSchema);
