const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const unidadMedidaSchema = new Schema({
  removed: {
    type: Boolean,
    default: false,
  },
  enabled: {
    type: Boolean,
    default: true,
  },
  abbreviation: {
    type: String,
    uppercase: true,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    // required: true,
  },
  description: {
    type: String,
    required: true,
  },
  color: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
  },
  created: {
    type: Date,
    default: Date.now,
  },
  active: {
    type: Boolean,
    default: true,
  },
});
unidadMedidaSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('UnitMeasurement', unidadMedidaSchema);
