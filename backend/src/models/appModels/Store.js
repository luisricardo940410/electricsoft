const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const articuloSchema = new Schema({
  removed: {
    type: Boolean,
    default: false,
  },
  enabled: {
    type: Boolean,
    default: true,
  },
  storeCode: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  name: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  description: {
    type: String,
    uppercase: true,
    required: true,
  },
  manager: {
    type: String,
    uppercase: true,
    // required: true,
  },
  adress1: {
    type: String,
    uppercase: true,
    // required: true,
  },
  address2: {
    type: String,
    uppercase: true,
    // required: true,
  },
  street: {
    type: String,
    uppercase: true,
    // required: true,
  },
  suburb: {
    type: String,
    uppercase: true,
    // required: true,
  },
  city: {
    type: String,
    uppercase: true,
    // required: true,
  },
  township: {
    type: String,
    uppercase: true,
    // required: true,
  },
  state: {
    type: String,
    uppercase: true,
    // required: true,
  },
  postalCode: {
    type: String,
    uppercase: true,
    // required: true,
  },
  country: {
    type: String,
    uppercase: true,
    // required: true,
  },
  satCountry: {
    type: String,
    default: 'MEX',
    uppercase: true,
  },
  phone1: {
    type: String,
    uppercase: true,
  },
  phone2: {
    type: String,
    uppercase: true,
  },
  rfc: {
    type: String,
    uppercase: true,
    // required: true,
  },

  type: {
    type: String,
    default: 'A',
    uppercase: true,
  },
  email: {
    type: String,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

articuloSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Store', articuloSchema);
