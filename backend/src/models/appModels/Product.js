const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  removed: {
    type: Boolean,
    default: false,
  },
  enabled: {
    type: Boolean,
    default: true,
  },

  productCategory: {
    type: mongoose.Schema.ObjectId,
    ref: 'ProductCategory',
    required: true,
    autopopulate: true,
  },
  brand: {
    type: mongoose.Schema.ObjectId,
    ref: 'Brand',
    // required: true,
    autopopulate: true,
  },
  suppliers: [{ type: mongoose.Schema.ObjectId, ref: 'Supplier' }],
  storeCode: {
    type: String,
    uppercase: true,
    trim: true,
    // required: true,
  },
  supplierCode: {
    type: String,
    uppercase: true,
    trim: true,
  },
  name: {
    type: String,
    uppercase: true,
    required: true,
  },
  description: String,
  number: {
    type: Number,
  },
  title: String,
  tags: [String],
  headerImage: String,
  photo: String,
  pdf: {
    type: String,
  },
  images: [
    {
      id: String,
      name: String,
      path: String,
      description: String,
      isPublic: {
        type: Boolean,
        default: false,
      },
    },
  ],
  files: [
    {
      id: String,
      name: String,
      path: String,
      description: String,
      isPublic: {
        type: Boolean,
        default: false,
      },
    },
  ],
  priceBeforeTax: {
    type: Number,
  },
  taxRate: { type: Number, default: 0 },
  price: {
    type: Number,
    required: true,
  },
  priceA: {
    type: Number,
  },
  priceB: {
    type: Number,
  },
  priceC: {
    type: Number,
  },
  priceD: {
    type: Number,
  },
  currency: {
    type: String,
    uppercase: true,
    required: true,
  },
  quantity: {
    type: Number,
    // required: true,
  },
  minimumQuantity: {
    type: Number,
    // required: true,
  },
  maximumQuantity: {
    type: Number,
    // required: true,
  },
  unitMeasurement: {
    type: mongoose.Schema.ObjectId,
    ref: 'UnitMeasurement',
    required: true,
    autopopulate: true,
  },
  customField: [
    {
      fieldName: {
        type: String,
        trim: true,
        lowercase: true,
      },
      fieldType: {
        type: String,
        trim: true,
        lowercase: true,
        default: 'string',
      },
      fieldValue: {},
    },
  ],
  created: {
    type: Date,
    default: Date.now,
  },
  updated: {
    type: Date,
    default: Date.now,
  },
  company:{
    type: mongoose.Schema.ObjectId,
    ref: 'Company',
    required: true,
    autopopulate: true,
  },
  isPublic: {
    type: Boolean,
    default: true,
  },
});

schema.plugin(require('mongoose-autopopulate'));

module.exports = mongoose.model('Product', schema);
