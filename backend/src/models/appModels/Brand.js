const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const articuloSchema = new Schema({
  removed: {
    type: Boolean,
    default: false,
  },
  enabled: {
    type: Boolean,
    default: true,
  },
  name: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  description: {
    type: String,
    uppercase: true,
    required: true,
  },
  discount: {
    type: Number,
    default: 0,
  },
  created: {
    type: Date,
    default: Date.now,
  },
});

articuloSchema.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('Brand', articuloSchema);
