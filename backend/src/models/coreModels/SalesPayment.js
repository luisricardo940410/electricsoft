const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// mongoose.Promise = global.Promise;

const paySchema = new Schema({
    folioNumber:{
      type:Number
    },
    orderId: {
      type: String,
      trim: true      
    },
    message:{
      type: String,
      trim: true 
    },
    amount:{
      type:Number
    },
    payNetResponse: {
      type: Object,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },                                 
  });
  
  module.exports = mongoose.model('SalesPayment', paySchema);
