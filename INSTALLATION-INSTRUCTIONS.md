## Getting started

#### Step 1: Clone the repository

```bash
git clone https://github.com/idurar/idurar-erp-crm.git
```

```bash
cd idurar-erp-crm
```

#### Step 2: Create Your MongoDB Account and Database Cluster

- Cree su propia cuenta de MongoDB visitando el sitio web de MongoDB y registrándose para obtener una nueva cuenta.

- Cree una nueva base de datos o clúster siguiendo las instrucciones proporcionadas en la documentación de MongoDB. Recuerde anotar el "URI de conexión a su aplicación" para la base de datos, ya que lo necesitará más adelante. Además, asegúrese de cambiar `<contraseña>` con su propia contraseña.

- agregue su dirección IP actual a la lista blanca de IP de la base de datos MongoDB para permitir conexiones (esto es necesario siempre que cambie su IP)

#### Step 3: Edit the Environment File

- Verifique un archivo llamado .env en el directorio /backend.

  Este archivo almacenará variables de entorno para que se ejecute el proyecto.

#### Step 4: Update MongoDB URI

En el archivo .env, busque la línea que dice:

`DATABASE="your-mongodb-uri"`

Reemplace "your-mongodb-uri" con el URI real de su base de datos MongoDB.

#### Step 5: Install Backend Dependencies

En su terminal, navegue hasta el directorio /backend

```bash
cd backend
```

the urn the following command to install the backend dependencies:

```bash
npm install
```

Este comando instalará todos los paquetes requeridos especificados en el archivo package.json.

#### Step 6: Run Setup Script

Mientras aún se encuentra en el directorio /backend del proyecto, ejecute el siguiente comando para ejecutar el script de instalación:

```bash
npm run setup
```

Este script de configuración puede realizar las migraciones de bases de datos necesarias o cualquier otra tarea de inicialización requerida para el proyecto.

#### Step 7: Run the Backend Server

En la misma terminal, ejecute el siguiente comando para iniciar el servidor backend:

```bash
npm run dev
```

Este comando iniciará el servidor backend y escuchará las solicitudes entrantes.

#### Step 8: Install Frontend Dependencies

Abra una nueva ventana de terminal y ejecute el siguiente comando para instalar las dependencias del frontend:

```bash
cd frontend
```

```bash
npm install
```

#### Step 9: Run the Frontend Server

Después de instalar las dependencias del frontend, ejecute el siguiente comando en la misma terminal para iniciar el servidor frontend:

```bash
npm run dev
```

Este comando iniciará el servidor frontend y podrá acceder al sitio web en localhost:3000 en su navegador web.

:exclamación: :advertencia:` Si encuentra un error de OpenSSL mientras ejecuta el servidor frontend, siga estos pasos adicionales:`

Reason behind error: This is caused by the node.js V17 compatible issues with OpenSSL, see [this](https://github.com/nodejs/node/issues/40547) and [this](https://github.com/webpack/webpack/issues/14532) issue on GitHub.

Motivo del error: esto se debe a problemas de compatibilidad de node.js V17 con OpenSSL; consulte [esto](https://github.com/nodejs/node/issues/40547) y [esto](https://github.com/webpack/webpack/issues/14532) problema en GitHub.


Pruebe uno de estos y el error se solucionará.

- > upgrade to Node.js v20.

- > Habilitar proveedor OpenSSL heredado

Así es como puede habilitar el proveedor OpenSSL heredado

- On Unix-like (Linux, macOS, Git bash, etc.)

```bash
export NODE_OPTIONS=--openssl-legacy-provider
```

- On Windows command prompt:

```bash
set NODE_OPTIONS=--openssl-legacy-provider
```

- On PowerShell:

```bash
$env:NODE_OPTIONS = "--openssl-legacy-provider"
```

Aquí hay [referencia](https://github.com/webpack/webpack/issues/14532#issuecomment-947012063) sobre cómo habilitar el proveedor OpenSSL heredado

Después de probar las soluciones anteriores, ejecute el siguiente comando

```bash
npm run dev
```


> Si aún tiene problemas, siga [este hilo de stackoverflow] (https://stackoverflow.com/questions/69692842/error-message-error0308010cdigital-envelope-routinesunsupported). Tiene tantos tipos diferentes de opiniones. Definitivamente tienes una solución después de revisar el hilo.