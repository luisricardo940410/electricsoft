import { Avatar, Popover, Button, Badge } from 'antd';

// import Notifications from '@/components/Notification';

import { RocketOutlined } from '@ant-design/icons';

import useLanguage from '@/locale/useLanguage';

export default function UpgradeButton({ icon= <RocketOutlined />, count= 1 }) {
  const translate = useLanguage();


  const Content = () => {
    return (
      <>
        <p>{translate('Do you need help on customize of this app')}</p>
        <Button
          type="primary"
          onClick={() => {
            window.open(`https://www.logom.com.mx/`);
          }}
        >
          {translate('Contact us')}
        </Button>
      </>
    );
  };

  return (
    <>
      {count > 0 ? (
        <Popover content={<Content />} title={translate('Customize this application')} trigger="click">
          <Badge count={count} size="small">
            <Avatar
              icon={icon}
              style={{
                color: '#f56a00',
                backgroundColor: '#FFF',
                float: 'right',
                marginTop: '5px',
                cursor: 'pointer',
              }}
            />
          </Badge>
        </Popover>
      ) : (
        <Avatar
          icon={icon}
          style={{
            color: '#f56a00',
            backgroundColor: '#FFF',
            float: 'right',
            marginTop: '5px',
            cursor: 'pointer',
          }}
        />
      )}
    </>
  );
}


