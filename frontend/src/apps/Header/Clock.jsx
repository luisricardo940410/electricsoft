import React, { useEffect, useState } from 'react';
import { Tag, Tooltip} from 'antd';
import { ClockCircleOutlined,} from '@ant-design/icons';
import useResponsive from '@/hooks/useResponsive';
import useLanguage from '@/locale/useLanguage';
import moment from 'moment';
import 'moment/locale/es'; // Importa el idioma español de Moment.js




export const Clock = () => {
    const { isMobile } = useResponsive();
    const translate = useLanguage();
    const [dataTime, setDataTime] = useState({
        diaSemana: moment().format('dddd'),
        fechaLarga: moment().format('DD [de] MMMM [del] YYYY'),
        hora:moment().format('HH:mm:ss a')
    });

    useEffect(() => {   
    
    const timer = setInterval(() => {
        moment.locale('es');
        const fechaActual = moment();
       
        setDataTime({
                diaSemana: fechaActual.locale('es').format('dddd'),
                fechaLarga: fechaActual.locale('es').format('DD [de] MMMM [del] YYYY'),
                dia: fechaActual.locale('es').format('DD'),
                mes: fechaActual.locale('es').format('MMMM'),
                anio: fechaActual.format('YYYY'),
                hora: fechaActual.format('HH:mm:ss a')
            });
            
        }, 1000);
        return () => clearInterval(timer);
    }, []);

    const renderDateTime = ({ diaSemana, dia, mes, anio }) => {
        const diaSemanaTranslated = diaSemana ? translate(diaSemana) : 'N/A';
        const diaFormatted = dia || 'N/A';
        const mesTranslated = mes ? translate(mes) : 'N/A';
        const anioFormatted = anio || 'N/A';
    
        return `${diaSemanaTranslated}, ${diaFormatted} de ${mesTranslated} del ${anioFormatted}\u00A0`;
      };

    
    return (
        <>
        <div style={{
          width: isMobile ? '90px' : '400px',
          float: 'right',
          marginTop: '-15px',
          cursor: 'pointer',
          direction: 'ltr',
        }}>

            <span style={{color: '#FFF'}}>
                {renderDateTime(dataTime)}
            </span> 
            <Tooltip title={dataTime.hora}>
                <Tag
                    // color="error"
                    color="#cd201f"
                    icon={<ClockCircleOutlined/>}
                    >
                    {dataTime.hora}
                    </Tag> 

            </Tooltip>
           
            <span style={{color: 'red'}}>

                
            </span>
        </div>
        </>
    )



}