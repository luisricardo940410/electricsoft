import { useLayoutEffect, Suspense } from 'react';
import { useEffect } from 'react';
import { selectAppSettings } from '@/redux/settings/selectors';
import { useDispatch, useSelector } from 'react-redux';

import { Layout } from 'antd';

import { useAppContext } from '@/context/appContext';

import Navigation from '@/apps/Navigation/NavigationContainer';
import ExpensesNav from '@/apps/Navigation/ExpensesNav';
import HeaderContent from '@/apps/Header/HeaderContainer';
import PageLoader from '@/components/PageLoader';

import { settingsAction } from '@/redux/settings/actions';
import { currencyAction } from '@/redux/currency/actions';
import { translateAction } from '@/redux/translate/actions';
import { companyAction } from '@/redux/company/actions';
import { selectSettings } from '@/redux/settings/selectors';


import AppRouter from '@/router/AppRouter';

import useResponsive from '@/hooks/useResponsive';

import storePersist from '@/redux/storePersist';
import { selectLangDirection } from '@/redux/translate/selectors';

export default function ErpCrmApp() {
  const { Content , Footer} = Layout;

  const { state: stateApp, appContextAction } = useAppContext();
  const { app } = appContextAction;
  const { isNavMenuClose, currentApp } = stateApp;

  const { isMobile } = useResponsive();

  const dispatch = useDispatch();

  useLayoutEffect(() => {
    dispatch(settingsAction.list({ entity: 'setting' }));
    dispatch(currencyAction.list());
  }, []);

  const appSettings = useSelector(selectAppSettings);

  const { isSuccess: settingIsloaded } = useSelector(selectSettings);

  useEffect(() => {
    const { loadDefaultLang } = storePersist.get('firstVisit');
    if (appSettings.electricsoft_app_language && !loadDefaultLang) {
      dispatch(translateAction.translate(appSettings.electricsoft_app_language));
      window.localStorage.setItem('firstVisit', JSON.stringify({ loadDefaultLang: true }));
    }
  }, [appSettings]);
  const langDirection = useSelector(selectLangDirection);

  if (settingIsloaded)
    return (
      <Layout hasSider style={{ flexDirection: langDirection === 'rtl' ? 'row-reverse' : 'row' }}>
        {/* {currentApp === 'default' ? <Navigation /> : <ExpensesNav />} */}
        <Navigation />

        {isMobile ? (
          <Layout style={{ marginLeft: 0 }}>
            <HeaderContent />
            <Suspense fallback={<PageLoader />}>
            
            <Content
              style={{
                margin: '40px auto 30px',
                overflow: 'initial',
                width: '100%',
                padding: '0 25px',
                maxWidth: 'none',
              }}
            >
              <AppRouter />
            </Content>
            </Suspense>
          </Layout>
        ) : (
          <Layout>
            <HeaderContent />
            <Suspense fallback={<PageLoader />}>
           
            <Content
              style={{
                margin: '40px auto 30px',
                overflow: 'initial',
                width: '100%',
                padding: '0 15px',
                // --maxWidth: 1500,
              }}
            >
              <AppRouter />
            </Content>
            
            <Footer
            style={{
              textAlign: 'center',
              borderTop: '1px solid rgba(0, 0, 0, 0.1)',
            }}
          >
            Electric Soft ©{new Date().getFullYear()} Creado por Yanux Software
          </Footer>
          </Suspense>
          </Layout>
        )}
      </Layout>
    );
  else return <PageLoader />;
}
