import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Button, Drawer, Layout, Menu } from 'antd';

import { useAppContext } from '@/context/appContext';

import useLanguage from '@/locale/useLanguage';
import logoTextElectrica from '@/style/images/logo-eb.png';
import logoTextIluminacion from '@/style/images/logo-ib.png';
import logoIconElectrica from '@/style/images/logo-mini.png';
import logoIconIluminacion from '@/style/images/logo-mini_ib.png';

import useResponsive from '@/hooks/useResponsive';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGauge,
  faHouseChimney,
  faBoxesPacking,
  faUser,
  faBuilding,
  faCommentDollar,
  faPeopleArrows,
  faArrowRightFromBracket,
  faUsersBetweenLines,
  faFileInvoiceDollar,
  faFileInvoice,
  faGears,
  faUsers,
  faMoneyBill,
  faFileLines,
  faBox,
  faShop,
  faPeopleCarryBox,
  faCircleUser,
  faRightFromBracket,
  faDollarSign,
  faHandHoldingDollar,
  faWeightHanging,
  faDolly,
  faGear,
  faStore,
  faRetweet,
  faTruckField,
  faMoneyCheckDollar,
  faCalculator,
  faCopyright,
  faClosedCaptioning,
  faLayerGroup,
  faWallet,
  faMoneyBillTransfer,
  faMoneyCheck,
  faUserTie,
  faUserGroup,
  faCashRegister,
  faCoins,
  faEnvelope,
  faCreditCard,
  faCircleInfo,
  faTruckMoving,
  faTruckFast,
  faBasketShopping,
  faCartShopping,
  faFileImport,
  faDownload,
  faTruckRampBox,
  faTruck,
  faCartArrowDown,
  faCartFlatbed,
  faTruckPickup
} from '@fortawesome/free-solid-svg-icons';

import {
  SettingOutlined,
  CustomerServiceOutlined,
  ContainerOutlined,
  FileSyncOutlined,
  DashboardOutlined,
  TagOutlined,
  TagsOutlined,
  UserOutlined,
  CreditCardOutlined,
  MenuOutlined,
  FileOutlined,
  ShopOutlined,
  FilterOutlined,
  WalletOutlined,
  ReconciliationOutlined,
} from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectLangDirection } from '@/redux/translate/selectors';
import { selectCompanydetail } from '@/redux/company/selectors'
import { icon } from '@fortawesome/fontawesome-svg-core';

const { Sider } = Layout;

export default function Navigation() {
  const { isMobile } = useResponsive();

  return isMobile ? <MobileSidebar /> : <Sidebar collapsible={true} />;
}

function Sidebar({ collapsible, isMobile = false }) {
  let location = useLocation();

  const { state: stateApp, appContextAction } = useAppContext();
  const { isNavMenuClose } = stateApp;
  const { navMenu } = appContextAction;
  const [showLogoApp, setLogoApp] = useState(isNavMenuClose);
  const [currentPath, setCurrentPath] = useState(location.pathname.slice(1));

  const translate = useLanguage();
  const navigate = useNavigate();

  const items = [
    //dashboard
    {
      key: 'dashboard',
      icon: <FontAwesomeIcon icon={faGauge} />,
      label: <Link to={'/'}>{translate('dashboard')}</Link>,
    },
    //inventory
    {
      key: 'inventory',
      icon: <FontAwesomeIcon icon={faBoxesPacking} />,
      label: translate('inventory'),
      children: [
        {
          key: 'product',
          icon: <FontAwesomeIcon icon={faBox} />,
          label: <Link to="/product">{translate('product_list')}</Link>
        },
        {
          key: 'categoryproduct',
          icon: <FontAwesomeIcon icon={faLayerGroup} />,
          label: <Link to={'/category/product'}>{translate('products_category')}</Link>,
        },
        {
          key: 'um',
          icon: <FontAwesomeIcon icon={faWeightHanging} />,
          label: <Link to="/unit">{translate('unit Measurement')}</Link>,
        },
        {
          key: 'brands',
          icon: <FontAwesomeIcon icon={faCopyright} />,
          label: <Link to="/brand">{translate('brands')}</Link>,
        },
        {
          key: 'subbrand',
          icon: <FontAwesomeIcon icon={faClosedCaptioning} />,
          label: <Link to="/sub/brand">{translate('sub brands')}</Link>
        },
        {
          key: 'price',
          icon: <FontAwesomeIcon icon={faDollarSign} />,
          label: <Link to="/price">{translate('Price List')}</Link>
        },
        {
          key: 'utilidades',
          icon: <FontAwesomeIcon icon={faHandHoldingDollar} />,
          label: <Link to="/utilidades">Utilidad</Link>

        },

      ]
    },
    //purcharse Order 
    {
      key: 'purcharse',
      icon: <FontAwesomeIcon icon={faCartArrowDown} />,
      label: translate('inventory'),
      children: [
        {
          key: 'purcharseOrder',
          icon: <FontAwesomeIcon icon={faCartShopping} />,
          label: <Link to="/product">{translate('purcharse_order')}</Link>
        },


      ]
    },
    //Importer
    {
      key: 'importer',
      icon: <FontAwesomeIcon icon={faDownload} />,
      label: translate('importer'),
      children: [
        {
          key: 'fileImport',
          icon: <FontAwesomeIcon icon={faFileImport} />,
          label: <Link to="/product">{translate('Importer')}</Link>
        },


      ]
    },
    //delivery
    {
      key: 'delivery',
      icon: <FontAwesomeIcon icon={faTruckRampBox} />,
      label: translate('delivery'),
      children: [
        {
          key: 'destribution',
          icon: <FontAwesomeIcon icon={faCartFlatbed} />,
          label: <Link to="/product">{translate('destribution')}</Link>
        },
        {
          key: 'shipments',
          icon: <FontAwesomeIcon icon={faTruck} />,
          label: <Link to="/product">{translate('shipments')}</Link>
        },




      ]
    },
    //sales
    {
      key: 'sales',
      icon: <FontAwesomeIcon icon={faCashRegister} />,
      label: translate('sales'),
      children: [
        // {
        //   key: 'sale',
        //   icon: <FontAwesomeIcon icon={faFileLines} />,
        //   label: <Link to={'/sale'}>{translate('sales_counter')}</Link>,
        // },
        {
          key: 'invoice',
          icon: <FontAwesomeIcon icon={faFileInvoiceDollar} />,
          label: <Link to={'/invoice'}>{translate('invoices')}</Link>,
        },
        {
          key: 'quote',
          icon: <FontAwesomeIcon icon={faFileLines} />,
          label: <Link to={'/quote'}>{translate('quotes')}</Link>,
        },
        {
          key: 'offer',
          icon: <FontAwesomeIcon icon={faCommentDollar} />,
          label: <Link to={'/offer'}>{translate('offers')}</Link>,
        },
        {
          key: 'people',
          icon: <FontAwesomeIcon icon={faUser} />,
          label: <Link to={'/people'}>{translate('peoples')}</Link>,
        },
        {
          key: 'customer',
          icon: <FontAwesomeIcon icon={faUserGroup} />,
          label: <Link to={'/customer'}>{translate('customers')}</Link>,
        },

      ]
    },
    //aconunting
    {
      key: 'accounting',
      icon: <FontAwesomeIcon icon={faFileInvoiceDollar} />,
      label: translate('accounting'),
      children: [

        {
          label: (<Link to="/supplier">{translate('supplier')}</Link>),
          key: 'supplier:1',
          icon: <FontAwesomeIcon icon={faTruckField} />
        },
        {
          key: 'expenses',
          icon: <FontAwesomeIcon icon={faMoneyBillTransfer} />,
          label: <Link to={'/expenses'}>{translate('expenses')}</Link>,
        },
        {
          key: 'expensesCategory',
          icon: <FontAwesomeIcon icon={faMoneyCheck} />,
          label: <Link to={'/category/expenses'}>{translate('expenses_Category')}</Link>,
        },
        {
          key: 'payment',
          icon: <FontAwesomeIcon icon={faWallet} />,
          label: <Link to={'/payment'}>{translate('payments')}</Link>,
        },
      ]
    },
    //store
    {
      key: 'store',
      icon: <FontAwesomeIcon icon={faStore} />,
      label: translate('Store'),
      children: [
        {
          label: (<Link to="/store" >{translate('store list')}</Link>),
          key: 'storelist',
          icon: <FontAwesomeIcon icon={faDolly} />,
        },
        {
          label: translate('warehouse transactions'),
          key: 'warehouse',
          icon: <FontAwesomeIcon icon={faPeopleCarryBox} />
        },
        {
          label: translate('purcharse order'),
          key: 'purcharse',
          icon: <FontAwesomeIcon icon={faBasketShopping} />
        },
        {
          label: translate('shipments'),
          key: 'shipments',
          icon: <FontAwesomeIcon icon={faTruckFast} />
        },
      ],
    },
    {
      key: 'lead',
      icon: <FontAwesomeIcon icon={faPeopleArrows} />,
      label: <Link to={'/lead'}>{translate('leads')}</Link>,
    },
    {
      key: 'employee',
      icon: <FontAwesomeIcon icon={faUserTie} />,
      label: <Link to={'/employee'}>{translate('employee')}</Link>,
    },
    //settings
    {
      label: translate('Settings'),
      key: 'settings',
      icon: <FontAwesomeIcon icon={faGears} />,
      children: [
        {
          key: 'admin',
          icon: <FontAwesomeIcon icon={faUserTie} />,
          label: <Link to={'/admin'}>{translate('admin')}</Link>,
        },
        {
          key: 'generalSettings',
          icon: <FontAwesomeIcon icon={faGear} />,
          label: <Link to={'/settings'}>{translate('settings')}</Link>,
        },
        {
          key: 'company',
          icon: <FontAwesomeIcon icon={faBuilding} />,
          label: <Link to={'/company'}>{translate('companies')}</Link>,
        },
        {
          key: 'currency',
          icon: <FontAwesomeIcon icon={faCoins} />,
          label: <Link to={'/settings/currency'}>{translate('currencies')}</Link>,
        },

        {
          key: 'emailTemplates',
          icon: <FontAwesomeIcon icon={faEnvelope} />,
          label: <Link to={'/email'}>{translate('email_templates')}</Link>,
        },
        {
          key: 'paymentMode',
          icon: <FontAwesomeIcon icon={faCreditCard} />,
          label: <Link to={'/payment/mode'}>{translate('payments_mode')}</Link>,
        },
        {
          key: 'taxes',
          icon: <FontAwesomeIcon icon={faHandHoldingDollar} />,
          label: <Link to={'/taxes'}>{translate('taxes')}</Link>,
        },
        {
          key: 'about',
          icon: <FontAwesomeIcon icon={faCircleInfo} />,
          label: <Link to={'/about'}>{translate('about')}</Link>,
        },
      ],
    },
  ];


  useEffect(() => {
    if (location)
      if (currentPath !== location.pathname) {
        if (location.pathname === '/') {
          setCurrentPath('dashboard');
        } else setCurrentPath(location.pathname.slice(1));
      }
  }, [location, currentPath]);

  useEffect(() => {
    if (isNavMenuClose) {
      setLogoApp(isNavMenuClose);
    }
    const timer = setTimeout(() => {
      if (!isNavMenuClose) {
        setLogoApp(isNavMenuClose);
      }
    }, 200);
    return () => clearTimeout(timer);
  }, [isNavMenuClose]);
  const onCollapse = () => {
    navMenu.collapse();
  };

  const langDirection = useSelector(selectLangDirection)
  const currentCompany = useSelector(selectCompanydetail)
  return (
    <Sider
      collapsible={collapsible}
      collapsed={collapsible ? isNavMenuClose : collapsible}
      onCollapse={onCollapse}
      className="navigation"
      width={256}
      style={{
        overflow: 'auto',
        height: '120vh',
        direction: langDirection,
        position: isMobile ? "absolute" : "relative",
        bottom: '1px',
        ...(!isMobile && {
          background: '#001529',
          border: '#001529',
          [langDirection === "rtl" ? "right" : "left"]: '0px',
          top: '0px',
          // borderRadius: '8px',
        }),
      }}
      theme={'dark'}
    >
      <div
        className="logo"
        onClick={() => navigate('/')}
        style={{
          cursor: 'pointer',
        }}
      >
        {/* cuando esta contraido */}
        {showLogoApp && (
          <img
            src={currentCompany.code === 'EB' ? logoIconElectrica : logoIconIluminacion}
            alt="Logo"
            style={currentCompany.code === 'EB'
              ? { marginTop: '3px', marginLeft: '-140px', height: '50px' }
              : { marginTop: '3px', marginLeft: '-140px', height: '50px' }
            }
          />
        )}

        {!showLogoApp && (
          <img
            src={currentCompany.code === 'EB' ? logoTextElectrica : logoTextIluminacion}
            alt="Logo"
            style={currentCompany.code === 'EB'
              ? { marginLeft: '-5px', marginTop: '-20px', width: '180px', height: '100px' }
              : { marginLeft: '-5px', marginTop: '-10px', width: '140px', height: '80px' }
            }
          />

        )}
      </div>
      <Menu
        items={items}
        mode="inline"
        theme={'dark'}
        selectedKeys={[currentPath]}
        style={{
          // background: 'none',
          border: 'none',
        }}
      />
    </Sider>
  );
}

function MobileSidebar() {
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(false);
  };
  const onClose = () => {
    setVisible(false);
  };

  const langDirection = useSelector(selectLangDirection)
  return (
    <>
      <Button
        type="text"
        size="large"
        onClick={showDrawer}
        className="mobile-sidebar-btn"


        style={{ [langDirection === "rtl" ? "marginRight" : "marginLeft"]: 25 }}
      >
        <MenuOutlined style={{ fontSize: 18 }} />
      </Button>
      <Drawer
        width={250}
        // contentWrapperStyle={{
        //   boxShadow: 'none',
        // }}
        style={{ backgroundColor: 'rgba(255, 255, 255, 0)' }}
        placement={langDirection === "rtl" ? "right" : "left"}

        closable={false}
        onClose={onClose}
        open={visible}

      >
        <Sidebar collapsible={false} isMobile={true} />
      </Drawer>

    </>
  );
}
