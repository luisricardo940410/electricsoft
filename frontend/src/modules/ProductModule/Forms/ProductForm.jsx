import { useState, useEffect, useRef } from 'react';
import dayjs from 'dayjs';
import { Form, Input, InputNumber, Button, Select, Divider, Row, Col, Upload, Image, Tabs, Card } from 'antd';
import { PlusOutlined, UploadOutlined } from '@ant-design/icons';
import AutoCompleteAsync from '@/components/AutoCompleteAsync';
import {beforeUpload} from '@/utils/helpers';

import ItemRow from '@/modules/ErpPanelModule/ItemRow';
import MoneyInputFormItem from '@/components/MoneyInputFormItem';
import { selectFinanceSettings } from '@/redux/settings/selectors';
import { selectCurrentItem } from '@/redux/crud/selectors';
import { useDate } from '@/settings';
import calculate from '@/utils/calculate';
import { useSelector } from 'react-redux';
import SelectAsync from '@/components/SelectAsync';
import { DatePicker } from 'antd';

import useLanguage from '@/locale/useLanguage';
import { FILE_BASE_URL } from '@/config/serverApiConfig';
import SelectCurrency from '@/components/SelectCurrency';
import { useMoney } from '@/settings';

export default function ProductForm({ subTotal = 0, current = null }) {
  const translate = useLanguage();
  const money = useMoney();
  const [activeTab, setActiveTab] = useState('1');

  // const beforeUpload = (file) => {
  //   const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  //   if (!isJpgOrPng) {
  //     message.error('You can only upload JPG/PNG file!');
  //   }
  //   const isLt2M = file.size / 1024 / 1024 < 5;
  //   if (!isLt2M) {
  //     message.error('Image must smaller than 5MB!');
  //   }
  //   return false;
  // };

  const handleTabChange = (key) => {
    setActiveTab(key);
  };
  const [, setLabels] = useState('');

  const items=[
    {
      key:'1',
      label:translate('General information'),
      children: (
      <>
      <Row gutter={[24, 24]}>
      {current?.name && (
        <Col className="gutter-row" span={5}>
          <Image
            className="last rigth"
            src={current?.photo ? FILE_BASE_URL + current?.photo : undefined}
            size={40}
            width="100%"
            height="auto"
            style={{
              color: '#f56a00',
              backgroundColor: current?.name ? 'none' : '#fde3cf',
              boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 6px 1px',
            }}
            fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
            alt={current?.name || 'Default Image'}
          >
            {current?.name.charAt(0).toUpperCase()}
          </Image>  
        </Col>
        )
        }
      <Row gutter={[24, 24]}>
        <Col className="gutter-row" span={24}>
          <Row gutter={[24, 24]}>
            <Col className="gutter-row" span={8}>
                <Form.Item label={translate('store Code')} name="storeCode">
                  <Input />
                </Form.Item>
            </Col>
            <Col className="gutter-row" span={8}>
                <Form.Item label={translate('supplier Code')} name="supplierCode">
                  <Input />
                </Form.Item>
            </Col>
          </Row>
            <Form.Item label={translate('name')} name="name">
              <Input />
            </Form.Item>
        </Col>

       
        <Col className="gutter-row" span={24}>
            <Form.Item label={translate('description')} name="description">
              <Input />
            </Form.Item>
        </Col>

      </Row>      
      </Row>
      <Row gutter={[24, 24]} style={{ marginTop: '24px' }}>
      <Col className="gutter-row" span={5}>
          <Form.Item
            name="suppliers"
            label={translate('supplier')}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <AutoCompleteAsync
              entity={'supplier'}
              displayLabels={['legalName']}
              searchFields={'name'}
              redirectLabel={'Añadir Proveedor'}
              withRedirect
              urlToRedirect={'/supplier'}

            />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={5}>
          <Form.Item
            name="productCategory"
            label={translate('product Category')}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <AutoCompleteAsync
              entity={'productCategory'}
              displayLabels={['name']}
              searchFields={'name'}
              redirectLabel={'Añadir Categoria'}
              withRedirect
              urlToRedirect={'/category/product'}

            />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={5}>
          <Form.Item
            name="unitMeasurement"
            label={translate('unit Measurement')}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <AutoCompleteAsync
              entity={'unitMeasurement'}
              displayLabels={['name']}
              searchFields={'name'}
              redirectLabel={'Añadir Unidad de medida'}
              withRedirect
              urlToRedirect={'/unit'}
            />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('quantity')}
            name="quantity"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber min={1} style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('minimum Quantity')}
            name="minimumQuantity"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber min={1} style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('maximum Quantity')}
            name="maximumQuantity"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber min={1} style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        {/* <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('year')}
            name="year"
            initialValue={currentYear}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={6}>
          <SelectCurrency />
        </Col>
        <Col className="gutter-row" span={4}>
          <Form.Item
            label={translate('status')}
            name="status"
            rules={[
              {
                required: false,
              },
            ]}
            initialValue={'draft'}
          >
            <Select
              options={[
                { value: 'draft', label: translate('Draft') },
                { value: 'pending', label: translate('Pending') },
                { value: 'sent', label: translate('Sent') },
                { value: 'accepted', label: translate('Accepted') },
                { value: 'declined', label: translate('Declined') },
              ]}
            ></Select>
          </Form.Item>
        </Col>

        <Col className="gutter-row" span={8}>
          <Form.Item
            name="date"
            label={translate('Date')}
            rules={[
              {
                required: true,
                type: 'object',
              },
            ]}
            initialValue={dayjs()}
          >
            <DatePicker style={{ width: '100%' }} format={dateFormat} />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={6}>
          <Form.Item
            name="expiredDate"
            label={translate('Expire Date')}
            rules={[
              {
                required: true,
                type: 'object',
              },
            ]}
            initialValue={dayjs().add(30, 'days')}
          >
            <DatePicker style={{ width: '100%' }} format={dateFormat} />
          </Form.Item>
        </Col> */}

      </Row>

      </>
      ),
    },
    {
      key:'2',
      label:translate('Price information'),
      children: (
      <>
      <Row gutter={[24, 24]}>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('price')}
            name="price"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
          <InputNumber
            className="moneyInput"
            min={0}
            controls={false}
            addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
            addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
          />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('price A')}
            name="priceA"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
          <InputNumber
            className="moneyInput"
            min={0}
            controls={false}
            addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
            addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
          />
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('price B')}
            name="priceB"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
          <InputNumber
            className="moneyInput"
            min={0}
            controls={false}
            addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
            addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
          />
          </Form.Item>
        </Col>
        
        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('price C')}
            name="priceC"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
          <InputNumber
            className="moneyInput"
            min={0}
            controls={false}
            addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
            addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
          />
          </Form.Item>
        </Col>

        <Col className="gutter-row" span={3}>
          <Form.Item
            label={translate('price D')}
            name="priceD"
            initialValue={0}
            // initialValue={lastNumber}
            rules={[
              {
                required: true,
              },
            ]}
          >
          <InputNumber
            className="moneyInput"
            min={0}
            controls={false}
            addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
            addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
          />
          </Form.Item>
        </Col>
        
        <Col className="gutter-row" span={6}>
          <SelectCurrency />
        </Col>

      </Row>


      </>
      ),
    },
    {
      key:'3',
      label:translate('Store'),
      children: (
      <>
      <Row gutter={[24, 24]} style={{ marginTop: '24px' }}>
        <Col className="gutter-row" span={5}>
            <Form.Item
              name="company"
              label={translate('Company')}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <AutoCompleteAsync
                entity={'company'}
                displayLabels={['name']}
                searchFields={'name'}
                redirectLabel={'Añadir Compañia'}
                withRedirect
                urlToRedirect={'/company'}

              />
            </Form.Item>
          </Col>
      </Row>
      
      
      </>)

    },
    {
      key:'4',
      label:translate('attachments'),
      children: (
      <>
      <Row gutter={[24, 24]}>
      {current?.name && (
          <>
          <Col className="gutter-row" span={5}>
          <Form.Item
            name="file"
            label={translate('Image')}
            valuePropName="fileList"
            getValueFromEvent={(e) => e.fileList}
          >
            <Upload
              beforeUpload={beforeUpload}
              listType="picture"
              accept="image/png, image/jpeg"
              maxCount={1}
            >
              <Button icon={<UploadOutlined />}>{translate('click_to_upload')}</Button>
            </Upload>
          </Form.Item>
        </Col>
        <Col className="gutter-row" span={5}>
            <Form.Item
              name="datasheet"
              label={translate('Datasheet')}
              valuePropName="fileList"
              getValueFromEvent={(e) => e.fileList}
            >
              <Upload
                beforeUpload={beforeUpload}
                listType="picture"
                accept="image/png, image/jpeg"
                maxCount={1}
                showUploadList={{ showPreviewIcon: false, showRemoveIcon: true }}
              >
                <Button icon={<UploadOutlined />}>{translate('click_to_upload')}</Button>
              </Upload>
            </Form.Item>
        </Col>
          </>
        )}
      </Row>

      </>
      ),

    }
  ];


  return (
    <>
    <Card style={{ padding: '20px' }}>
    <Tabs defaultActiveKey={activeTab} items={items} onChange={handleTabChange} />

    </Card>
    

    </>
  );
}
