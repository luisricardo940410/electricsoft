import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Form, Input, InputNumber, Row, Col } from 'antd';

import { DeleteOutlined } from '@ant-design/icons';
import { useMoney, useDate } from '@/settings';
import calculate from '@/utils/calculate';
import useOnFetch from '@/hooks/useOnFetch';
import {IconButton} from '@mui/material';

import AutoCompleteImageAsync from '@/components/AutoCompleteImageAsync';
import { request } from '@/request';

export default function ItemRow({ field, remove, current = null , handleInputChange, form }) {

  const [totalState, setTotal] = useState(undefined);
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(0);
  const [stock, setStock] = useState(0);
  const [description, setDescription]= useState('')
  const [id, setId] = useState(0);
  let { onFetch, result, isSuccess, isLoading } = useOnFetch();

  const money = useMoney();

  const updateQt = (value) => {
    setQuantity(value);
  };
  
  const updatePrice = (value) => {
    setPrice(value);
  };

  useEffect(() => {
    if (current) {
      // When it accesses the /payment/ endpoint,
      // it receives an invoice.item instead of just item
      // and breaks the code, but now we can check if items exists,
      // and if it doesn't we can access invoice.items.

      const { items, invoice } = current;

      if (invoice) {
        const item = invoice[field.fieldKey];

        console.log(item)

        if (item) {
          setQuantity(item.quantity);
          setPrice(item.price);
        }
      } else {
        const item = items[field.fieldKey];

        if (item) {
          setQuantity(item.quantity);
          setPrice(item.price);
        }
      }
    }
  }, [current]);


  useEffect(() => {
    const currentTotal = calculate.multiply(price, quantity);
    setTotal(currentTotal);
  }, [quantity]);

  const asyncSearch = async () => {
    return await request.read({ entity:'product', id });
  };


  useEffect(() => {

    if(id!==0){
      const callback = asyncSearch();
      onFetch(callback);
    }

  }, [id]);


  useEffect(() => {
    if (result && id) {
      
      const {price,description} = result;
      console.log( {price,description})
      if (price !== undefined || description !== undefined ) {
        setStock(1)
        setPrice(price);
        setDescription(description)
        setTotal(calculate.multiply(price, quantity));
        handleInputChange(field.name, {price,description})
      }
    }
  }, [ id, result, field.name,form]);


  const handletSelect = (value) => {
    console.log(value)
    setId(value);
  };

  return (
    <Row gutter={[12, 12]} style={{ position: 'relative' }}>
      <Col className="gutter-row" span={7}>
        <Form.Item
          name={[field.name, 'product']}
          rules={[
            {
              required: true,
              message: 'Missing itemName name',
            },
            {
              pattern: /^(?!\s*$)[\s\S]+$/, // Regular expression to allow spaces, alphanumeric, and special characters, but not just spaces
              message: 'Item Name must contain alphanumeric or special characters',
            },
          ]}
        >
          {/* <Input placeholder="Item Name" /> */}
          <AutoCompleteImageAsync
              entity={'product'}
              displayLabels={['name']}
              searchFields={['storeCode','name']}
              redirectLabel={'Add New product'}
              withRedirect
              urlToRedirect={'/product'}
              onChange={handletSelect}
            />
        </Form.Item>
      </Col>
      <Col className="gutter-row" span={2}>
        <Form.Item name={[field.name, 'stock']}  Key={[field.fieldKey, 'stock']} initialValue={0}>
          <Form.Item>
              <InputNumber style={{ width: '100%' }} readOnly value={stock}min={0} />
            </Form.Item>
        </Form.Item>
      </Col>
      <Col className="gutter-row" span={7}>
        <Form.Item name={[field.name, 'description']}>
          <Form.Item>
            <Input placeholder="description Name" value={description} />
          </Form.Item>
        </Form.Item>
      </Col>
      <Col className="gutter-row" span={2}>
        <Form.Item name={[field.name, 'quantity']} Key={[field.fieldKey, 'quantity']}  rules={[{ required: true }]} initialValue={0}>
          <InputNumber style={{ width: '100%' }} min={0} onChange={updateQt} />
        </Form.Item>
      </Col>
      <Col className="gutter-row" span={2}>
        <Form.Item name={[field.name, 'price']}  Key={[field.fieldKey, 'price']} initialValue={0}>
          <Form.Item>
              <InputNumber
                readOnly
                className="moneyInput"
                value={price}
                min={0}
                controls={false}
                addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
                addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
                formatter={(value) =>
                  money.amountFormatter({ amount: value, currency_code: money.currency_code })
                }
              />
            </Form.Item>
        </Form.Item>
      </Col>
      <Col className="gutter-row" span={2}>
        <Form.Item name={[field.name, 'total']}>
          <Form.Item>
            <InputNumber
              readOnly
              className="moneyInput"
              value={totalState}
              min={0}
              controls={false}
              addonAfter={money.currency_position === 'after' ? money.currency_symbol : undefined}
              addonBefore={money.currency_position === 'before' ? money.currency_symbol : undefined}
              formatter={(value) =>
                money.amountFormatter({ amount: value, currency_code: money.currency_code })
              }
            />
          </Form.Item>
        </Form.Item>
      </Col>

      <div style={{ position: 'absolute', right: '-20px', top: ' 5px' }}>
        <IconButton color='error'  onClick={() => remove(field.name)}>
        <DeleteOutlined />

      </IconButton>
        
      </div>
    </Row>
  );
}
