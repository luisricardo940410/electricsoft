import { useState, useEffect } from 'react';
import { Divider } from 'antd';

import { Button, Row, Col, Descriptions, Statistic, Tag, Card, Avatar, Image } from 'antd';
import { PageHeader } from '@ant-design/pro-layout';
import {
  EditOutlined,
  FilePdfOutlined,
  CloseCircleOutlined,
  RetweetOutlined,
  MailOutlined,
} from '@ant-design/icons';

import { useSelector, useDispatch } from 'react-redux';
import useLanguage from '@/locale/useLanguage';
import { erp } from '@/redux/erp/actions';

import { generate as uniqueId } from 'shortid';

import { selectCurrentItem } from '@/redux/erp/selectors';

import { DOWNLOAD_BASE_URL } from '@/config/serverApiConfig';
import { useMoney, useDate } from '@/settings';
import { FILE_BASE_URL } from '@/config/serverApiConfig';
import useMail from '@/hooks/useMail';
import { useNavigate } from 'react-router-dom';
import { tagColor } from '@/utils/statusTagColor';
import { settingsAction } from '@/redux/settings/actions';
import { selectCompanyCode } from '@/redux/company/selectors';

const Item = ({ item, currentErp }) => {
  const { moneyFormatter } = useMoney();
  return (
    <Row gutter={[12, 0]} key={item._id}>
      <Col className="gutter-row" span={11}>
        <p style={{ marginBottom: 5 }}>
          <strong>{item.itemName}</strong>
        </p>
        <p>
          <Avatar
            className="last"
            // src={item.product.photo ? FILE_BASE_URL + item.product?.photo : undefined}
            src={
              item.product?.photo
                ?
                <Image
                  src={item.product.photo ? FILE_BASE_URL + item.product?.photo : undefined}
                  width={40}
                  height={40}
                  preview={true} // Disable preview
                />
                :
                undefined
            }
            style={{
              color: '#f56a00',
              backgroundColor: item.product.photo ? 'none' : '#fde3cf',
              boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 10px 2px',
              cursor: 'pointer',
              marginRight: '30px', 
            }}
            size="large"
          >
            {item.product.name?.charAt(0)?.toUpperCase()}
          </Avatar>
          <>{item.product.storeCode + ' - '}<span>{item.product.name}</span></>
        </p>
      </Col>
      <Col className="gutter-row" span={4}>
        <p
          style={{
            textAlign: 'right',
          }}
        >
          {moneyFormatter({ amount: item.price, currency_code: currentErp.currency })}
        </p>
      </Col>
      <Col className="gutter-row" span={4}>
        <p
          style={{
            textAlign: 'right',
          }}
        >
          {item.quantity}
        </p>
      </Col>
      <Col className="gutter-row" span={5}>
        <p
          style={{
            textAlign: 'right',
            fontWeight: '700',
          }}
        >
          {moneyFormatter({ amount: item.total, currency_code: currentErp.currency })}
        </p>
      </Col>
      <Divider dashed style={{ marginTop: 0, marginBottom: 15 }} />
    </Row>
  );
};

export default function ReadItem({ config, selectedItem }) {
  const translate = useLanguage();
  const { entity, ENTITY_NAME } = config;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { moneyFormatter } = useMoney();
  const { send, isLoading: mailInProgress } = useMail({ entity });

  const { result: currentResult } = useSelector(selectCurrentItem);

  const resetErp = {
    status: '',
    client: {
      name: '',
      email: '',
      phone: '',
      address: '',
    },
    subTotal: 0,
    taxTotal: 0,
    taxRate: 0,
    total: 0,
    credit: 0,
    number: 0,
    year: 0,
  };

  const [itemslist, setItemsList] = useState([]);
  const [currentErp, setCurrentErp] = useState(selectedItem ?? resetErp);
  const [client, setClient] = useState({});
  const company = useSelector(selectCompanyCode)

  const updateCurrency = (value) => {
    dispatch(
      settingsAction.updateCurrency({
        data: { default_currency_code: value },
      })
    );
  };
  
  useEffect(() => {
    if (currentResult) {
      const { items, invoice, ...others } = currentResult;

      if (items) {
        setItemsList(items);
        setCurrentErp(currentResult);
      } else if (invoice.items) {
        setItemsList(invoice.items);
        setCurrentErp({ ...invoice.items, ...others, ...invoice });
      }
      updateCurrency(currentResult.currency);
    }
    return () => {
      setItemsList([]);
      setCurrentErp(resetErp);
    };
  }, [currentResult]);

  useEffect(() => {
    if (currentErp?.client) {
      setClient(currentErp.client[currentErp.client.type]);
    }
  }, [currentErp]);

  return (
    <>
      <PageHeader
        onBack={() => {
          navigate(`/${entity.toLowerCase()}`);
        }}
        title={<span style={{ color: '#F0EBE3' }}>{`${ENTITY_NAME} # ${currentErp.number}/${currentErp.year || ''}`}</span>}
        ghost={false}
        tags={[
          <Tag color={tagColor(currentErp.status)?.color} key="status">
            {currentErp.status && translate(currentErp.status)}
          </Tag>,
          currentErp.paymentStatus && (
            <Tag color={tagColor(currentErp.paymentStatus)?.color} key="paymentStatus">
              {currentErp.paymentStatus && translate(currentErp.paymentStatus)}
            </Tag>
          ),
        ]}
        extra={[
          <Button
            key={`${uniqueId()}`}
            onClick={() => {
              navigate(`/${entity.toLowerCase()}`);
            }}
            icon={<CloseCircleOutlined />}
          >
            {translate('Close')}
          </Button>,
          <Button
            key={`${uniqueId()}`}
            onClick={() => {
              window.open(
                `${DOWNLOAD_BASE_URL}${entity}/${entity}-${currentErp._id}.pdf`,
                '_blank'
              );
            }}
            icon={<FilePdfOutlined />}
          >
            {translate('Download PDF')}
          </Button>,
          <Button
            key={`${uniqueId()}`}
            loading={mailInProgress}
            onClick={() => {
              send(currentErp._id);
            }}
            icon={<MailOutlined />}
          >
            {translate('Send by Email')}
          </Button>,
          <Button
            key={`${uniqueId()}`}
            onClick={() => {
              dispatch(erp.convert({ entity, id: currentErp._id }));
            }}
            icon={<RetweetOutlined />}
            style={{ display: (entity === 'quote' || entity === 'sale') ? 'inline-block' : 'none' }}
          >
            {translate('Convert to Invoice')}
          </Button>,

          <Button
            key={`${uniqueId()}`}
            onClick={() => {
              dispatch(
                erp.currentAction({
                  actionType: 'update',
                  data: currentErp,
                })
              );
              navigate(`/${entity.toLowerCase()}/update/${currentErp._id}`);
            }}
            type="primary"
            icon={<EditOutlined />}
          >
            {translate('Edit')}
          </Button>,
        ]}

        valueStyle={{ color: '#F0EBE3' }}
        style={{
          color: '#F0EBE3',
          padding: '20px 15px',
          // direction:langDirection,
          background: company === 'EB' ? '#E64848' : '#EF9C66'
        }}
      >
        <Row>
          <Statistic
            title={<span style={{ color: '#F0EBE3' }}>{translate("Status")}</span>}
            value={currentErp.status}
            valueStyle={{ color: '#F0EBE3' }} />

          <Statistic
            title={<span style={{ color: '#F0EBE3' }}>{translate('SubTotal')}</span>}
            value={moneyFormatter({
              amount: currentErp.subTotal,
              currency_code: currentErp.currency,
            })}
            valueStyle={{ color: '#F0EBE3' }}
            style={{
              margin: '0 32px',
              color: '#F0EBE3',
            }}
          />
          <Statistic
            title={<span style={{ color: '#F0EBE3' }}>{translate('Total')}</span>}
            value={moneyFormatter({ amount: currentErp.total, currency_code: currentErp.currency })}
            valueStyle={{ color: '#F0EBE3' }}
            style={{
              margin: '0 32px',
            }}
          />
          <Statistic
            title={<span style={{ color: '#F0EBE3' }}>{translate('Paid')}</span>}
            value={moneyFormatter({
              amount: currentErp.credit,
              currency_code: currentErp.currency,
            })}
            valueStyle={{ color: '#F0EBE3' }}
            style={{
              margin: '0 32px',
            }}
          />
        </Row>
      </PageHeader>
      <Divider dashed />
      <Card style={{ padding: '20px' }}>
        <Descriptions title={`${translate('Client')} : ${currentErp.client.name}`}>
          <Descriptions.Item label={translate('Address')}>{client.address}</Descriptions.Item>
          <Descriptions.Item label={translate('email')}>{client.email}</Descriptions.Item>
          <Descriptions.Item label={translate('Phone')}>{client.phone}</Descriptions.Item>
        </Descriptions>
        <Divider />
        <Row gutter={[12, 0]}>
          <Col className="gutter-row" span={11}>
            <p>
              <strong>{translate('Product')}</strong>
            </p>
          </Col>
          <Col className="gutter-row" span={4}>
            <p
              style={{
                textAlign: 'right',
              }}
            >
              <strong>{translate('Price')}</strong>
            </p>
          </Col>
          <Col className="gutter-row" span={4}>
            <p
              style={{
                textAlign: 'right',
              }}
            >
              <strong>{translate('Quantity')}</strong>
            </p>
          </Col>
          <Col className="gutter-row" span={5}>
            <p
              style={{
                textAlign: 'right',
              }}
            >
              <strong>{translate('Total')}</strong>
            </p>
          </Col>
          <Divider />
        </Row>
        {itemslist.map((item) => (
          <Item key={item._id} item={item} currentErp={currentErp}></Item>
        ))}
        <div
          style={{
            width: '300px',
            float: 'right',
            textAlign: 'right',
            fontWeight: '700',
          }}
        >
          <Row gutter={[12, -5]}>
            <Col className="gutter-row" span={12}>
              <p>{translate('Sub Total')} :</p>
            </Col>

            <Col className="gutter-row" span={12}>
              <p>
                {moneyFormatter({ amount: currentErp.subTotal, currency_code: currentErp.currency })}
              </p>
            </Col>
            <Col className="gutter-row" span={12}>
              <p>
                {translate('Tax Total')} ({currentErp.taxRate} %) :
              </p>
            </Col>
            <Col className="gutter-row" span={12}>
              <p>
                {moneyFormatter({ amount: currentErp.taxTotal, currency_code: currentErp.currency })}
              </p>
            </Col>
            <Col className="gutter-row" span={12}>
              <p>{translate('Total')} :</p>
            </Col>
            <Col className="gutter-row" span={12}>
              <p>
                {moneyFormatter({ amount: currentErp.total, currency_code: currentErp.currency })}
              </p>
            </Col>
          </Row>
        </div>
      </Card>
    </>
  );
}
