import { useEffect } from 'react';
import {
  EyeOutlined,
  EditOutlined,
  DeleteOutlined,
  FilePdfOutlined,
  RedoOutlined,
  PlusOutlined,
  EllipsisOutlined,
  ArrowRightOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';

// material components
import { Box, IconButton, Tooltip, } from '@mui/material';
import {MaterialReactTable,useMaterialReactTable} from 'material-react-table';

import { Button, Divider } from 'antd';
import { PageHeader } from '@ant-design/pro-layout';

import AutoCompleteAsync from '@/components/AutoCompleteAsync';
import { useSelector, useDispatch } from 'react-redux';
import useLanguage from '@/locale/useLanguage';
import { erp } from '@/redux/erp/actions';
import { selectListItems } from '@/redux/erp/selectors';
import {selectCompanydetail,selectCompanyCode, selectCurrentCompany} from '@/redux/company/selectors';

import { useErpContext } from '@/context/erp';
import { generate as uniqueId } from 'shortid';
import { useNavigate } from 'react-router-dom';

import { DOWNLOAD_BASE_URL } from '@/config/serverApiConfig';
import { selectLangDirection } from '@/redux/translate/selectors';


function AddNewItem({ config }) {
  const navigate = useNavigate();
  const { ADD_NEW_ENTITY, entity } = config;

  const handleClick = () => {
    navigate(`/${entity.toLowerCase()}/create`);
  };

  return (
    <Button onClick={handleClick} type="primary" icon={<PlusOutlined />}>
      {ADD_NEW_ENTITY}
    </Button>
  );
}

export default function DataTable({ config, extra = [] }) {
  const translate = useLanguage();
  let { entity, dataTableColumns, disableAdd = false, searchConfig } = config;

  const { DATATABLE_TITLE } = config;

  const { result: listResult, isLoading: listIsLoading } = useSelector(selectListItems);
  const   currentCompany = useSelector(selectCompanydetail);
  console.log('currentCompany',currentCompany);
  const company = useSelector(selectCompanyCode)

  const { pagination, items: dataSource } = listResult;

  const { erpContextAction } = useErpContext();
  const { modal } = erpContextAction;

  const items = [
    {
      label: 'Show',
      key: 'read',
      icon: <EyeOutlined />,
      color: 'secondary',
    },
    {
      label: 'Edit',
      key: 'edit',
      icon: <EditOutlined />,
      color: 'primary',
    },
    {
      label: 'Delete',
      key: 'delete',
      icon: <DeleteOutlined />,
      color: 'error',
    },
    // Add any extra items here
    ...extra,
  ];

  const navigate = useNavigate();

  const handleRead = (record) => {
    dispatch(erp.currentItem({ data: record }));
    navigate(`/${entity}/read/${record._id}`);
  };
  const handleEdit = (record) => {
    const data = { ...record };
    dispatch(erp.currentAction({ actionType: 'update', data }));
    navigate(`/${entity}/update/${record._id}`);
  };
  const handleDownload = (record) => {
    window.open(`${DOWNLOAD_BASE_URL}${entity}/${entity}-${record._id}.pdf`, '_blank');
  };

  const handleDelete = (record) => {
    dispatch(erp.currentAction({ actionType: 'delete', data: record }));
    modal.open();
  };

  const handleRecordPayment = (record) => {
    dispatch(erp.currentItem({ data: record }));
    navigate(`/invoice/pay/${record._id}`);
  };

  dataTableColumns = [
    ...dataTableColumns,
    {
      header: 'Action',
      key: 'mrt-row-actions',
      Cell: ({ cell, row }) => (
      <Box>
      {items.map((item) => (
        <Tooltip key={item.key} title={item.label}>
          <IconButton color={item.color} onClick={() => {
             switch (item.key) {
                case 'read':
                  handleRead(row.original);
                  break;
                case 'edit':
                  handleEdit(row.original);
                  break;
                case 'download':
                  handleDownload(record);
                  break;
                case 'delete':
                  handleDelete(row.original);
                  break;
                case 'recordPayment':
                  handleRecordPayment(row.original);
                  break;
                default:
                  break;
            }
          }}>
            {item.icon}
          </IconButton>
        </Tooltip>
      ))}
      </Box>
      ),
    },
  ];

  const dispatch = useDispatch();

  const handelDataTableLoad = (pagination) => {
    const options = { page: pagination.current || 1, items: pagination.pageSize || 10, fields:'company',q:currentCompany.id };
    dispatch(erp.list({ entity, options }));
  };

  const dispatcher = () => {
    const options = { fields:'company',q:currentCompany.id };
    dispatch(erp.list({ entity ,options}));
  };

  useEffect(() => {
    const controller = new AbortController();
    dispatcher();
    return () => {
      controller.abort();
    };
  }, [currentCompany]);

  const filterTable = (value) => {
    const options = { filter: searchConfig?.searchFields, equal:value, fields:'company',q:currentCompany.id };
    dispatch(erp.list({ entity, options }));
  };
  const langDirection=useSelector(selectLangDirection)

  const table = useMaterialReactTable({
    columns:dataTableColumns,
    data:dataSource,
    enableGlobalFilter:false,
    initialState: { 
      enableGlobalFilterModes:false,
      showGlobalFilter: false,
      showColumnFilters: false,
      pagination: pagination,
      density:'compact'
    },
    state: {
      isLoading:listIsLoading,
    },
    paginationDisplayMode: 'pages',
    positionToolbarAlertBanner: 'bottom',
    muiPaginationProps: {
      color: 'primary',
      rowsPerPageOptions: [10, 100, 500],
      shape: 'rounded',
      variant: 'outlined',
    },
  });

  return (
    <>
      <PageHeader
        title={<span style={{ color: '#F0EBE3' }}>{`${DATATABLE_TITLE}`}</span>} // Aquí añadimos el estilo para el título
        ghost={true}
        onBack={() => window.history.back()}
        backIcon={langDirection==="rtl"?<ArrowRightOutlined/>:<ArrowLeftOutlined />}
        extra={[
          <AutoCompleteAsync
            key={`${uniqueId()}`}
            entity={searchConfig?.entity}
            displayLabels={['name']}
            searchFields={'name'}
            onChange={filterTable}
            // redirectLabel={'Add New Client'}
            // withRedirect
            // urlToRedirect={'/customer'}
          />,
          <Button onClick={handelDataTableLoad} key={`${uniqueId()}`} icon={<RedoOutlined />}>
            {translate('Refresh')}
          </Button>,

          !disableAdd && <AddNewItem config={config} key={`${uniqueId()}`} />,
        ]}
        style={{
          padding: '20px 15px',
          direction:langDirection,
          color: '#F0EBE3',
          background: currentCompany.color
          // company ==='EB'?'#E64848': '#EF9C66'
        }}
      ></PageHeader>
      

      <Divider dashed />
    <Box
      sx={{
        padding: '20px', // Ajusta el valor de padding según tus necesidades
      }}
    >
      <MaterialReactTable table={table} />
    </Box>
    </>
  );
}

// import { useEffect } from 'react';
// import {
//   EyeOutlined,
//   EditOutlined,
//   DeleteOutlined,
//   FilePdfOutlined,
//   RedoOutlined,
//   PlusOutlined,
//   EllipsisOutlined,
//   ArrowRightOutlined,
//   ArrowLeftOutlined,
// } from '@ant-design/icons';

// // material components
// import { Box, IconButton, Tooltip } from '@mui/material';
// import { MaterialReactTable, useMaterialReactTable } from 'material-react-table';

// import { Button, Divider } from 'antd';
// import { PageHeader } from '@ant-design/pro-layout';

// import AutoCompleteAsync from '@/components/AutoCompleteAsync';
// import { useSelector, useDispatch } from 'react-redux';
// import useLanguage from '@/locale/useLanguage';
// import { erp } from '@/redux/erp/actions';
// import { selectListItems } from '@/redux/erp/selectors';
// import { useErpContext } from '@/context/erp';
// import { generate as uniqueId } from 'shortid';
// import { useNavigate } from 'react-router-dom';

// import { DOWNLOAD_BASE_URL } from '@/config/serverApiConfig';
// import { selectLangDirection } from '@/redux/translate/selectors';
// import { selectCompanyCode } from '@/redux/company/selectors';

// function AddNewItem({ config }) {
//   const navigate = useNavigate();
//   const { ADD_NEW_ENTITY, entity } = config;

//   const handleClick = () => {
//     navigate(`/${entity.toLowerCase()}/create`);
//   };

//   return (
//     <Button onClick={handleClick} type="primary" icon={<PlusOutlined />}>
//       {ADD_NEW_ENTITY}
//     </Button>
//   );
// }

// export default function DataTable({ config, extra = [] }) {
//   const translate = useLanguage();
//   let { entity, dataTableColumns, disableAdd = false, searchConfig } = config;

//   const { DATATABLE_TITLE } = config;

//   const { result: listResult, isLoading: listIsLoading } = useSelector(selectListItems);
//   const company = useSelector(selectCompanyCode);

//   const { pagination, items: dataSource } = listResult;

//   const { erpContextAction } = useErpContext();
//   const { modal } = erpContextAction;

//   const items = [
//     {
//       label: 'Show',
//       key: 'read',
//       icon: <EyeOutlined />,
//       color: 'secondary',
//     },
//     {
//       label: 'Edit',
//       key: 'edit',
//       icon: <EditOutlined />,
//       color: 'primary',
//     },
//     {
//       label: 'Delete',
//       key: 'delete',
//       icon: <DeleteOutlined />,
//       color: 'error',
//     },
//     // Add any extra items here
//     ...extra,
//   ];

//   const navigate = useNavigate();

//   const handleRead = (record) => {
//     dispatch(erp.currentItem({ data: record }));
//     navigate(`/${entity}/read/${record._id}`);
//   };
//   const handleEdit = (record) => {
//     const data = { ...record };
//     dispatch(erp.currentAction({ actionType: 'update', data }));
//     navigate(`/${entity}/update/${record._id}`);
//   };
//   const handleDownload = (record) => {
//     window.open(`${DOWNLOAD_BASE_URL}${entity}/${entity}-${record._id}.pdf`, '_blank');
//   };

//   const handleDelete = (record) => {
//     dispatch(erp.currentAction({ actionType: 'delete', data: record }));
//     modal.open();
//   };

//   const handleRecordPayment = (record) => {
//     dispatch(erp.currentItem({ data: record }));
//     navigate(`/invoice/pay/${record._id}`);
//   };

//   dataTableColumns = [
//     ...dataTableColumns,
//     {
//       header: 'Action',
//       key: 'mrt-row-actions',
//       Cell: ({ row }) => (
//         <Box>
//           {items.map((item) => (
//             <Tooltip key={item.key} title={item.label}>
//               <IconButton color={item.color} onClick={() => {
//                 switch (item.key) {
//                   case 'read':
//                     handleRead(row.original);
//                     break;
//                   case 'edit':
//                     handleEdit(row.original);
//                     break;
//                   case 'download':
//                     handleDownload(row.original);
//                     break;
//                   case 'delete':
//                     handleDelete(row.original);
//                     break;
//                   case 'recordPayment':
//                     handleRecordPayment(row.original);
//                     break;
//                   default:
//                     break;
//                 }
//               }}>
//                 {item.icon}
//               </IconButton>
//             </Tooltip>
//           ))}
//         </Box>
//       ),
//     },
//   ];

//   const dispatch = useDispatch();

//   const handelDataTableLoad = (pagination) => {
//     const options = { page: pagination.current || 1, items: pagination.pageSize || 10 };
//     dispatch(erp.list({ entity, options }));
//   };

//   const dispatcher = () => {
//     dispatch(erp.list({ entity }));
//   };

//   useEffect(() => {
//     const controller = new AbortController();
//     dispatcher();
//     return () => {
//       controller.abort();
//     };
//   }, []);

//   const filterTable = (value) => {
//     console.log('search', value);
//     const options = { q: value, filter: searchConfig?.entity };
//     dispatch(erp.list({ entity, options }));
//   };
//   const langDirection = useSelector(selectLangDirection);

//   const table = useMaterialReactTable({
//     columns: dataTableColumns,
//     data: dataSource,
//     enableGlobalFilter: false,
//     initialState: {
//       enableGlobalFilterModes: false,
//       showGlobalFilter: false,
//       showColumnFilters: false,
//       pagination: pagination,
//       density: 'compact',
//     },
//     state: {
//       isLoading: listIsLoading,
//     },
//     paginationDisplayMode: 'pages',
//     positionToolbarAlertBanner: 'bottom',
//     muiPaginationProps: {
//       color: 'primary',
//       rowsPerPageOptions: [10, 100, 500],
//       shape: 'rounded',
//       variant: 'outlined',
//     },
//   });

//   return (
//     <>
//       <PageHeader
//         title={`${DATATABLE_TITLE} ${company}`}
//         ghost={true}
//         onBack={() => window.history.back()}
//         backIcon={langDirection === "rtl" ? <ArrowRightOutlined /> : <ArrowLeftOutlined />}
//         extra={[
//           <AutoCompleteAsync
//             key={`${uniqueId()}`}
//             entity={searchConfig?.entity}
//             displayLabels={['name']}
//             searchFields={'name'}
//             onChange={filterTable}
//           />,
//           <Button onClick={handelDataTableLoad} key={`${uniqueId()}`} icon={<RedoOutlined />}>
//             {translate('Refresh')}
//           </Button>,
//           !disableAdd && <AddNewItem config={config} key={`${uniqueId()}`} />,
//         ]}
//         style={{
//           padding: '20px 15px',
//           direction: langDirection,
//           background: company === 'EB' ? '#FF8080' : '#F1C93B', // Asegúrate de que los valores sean cadenas de texto correctas
//         }}
//       />
//       <Divider dashed />

//       <Box
//         sx={{
//           padding: '20px', // Ajusta el valor de padding según tus necesidades
//         }}
//       >
//         <MaterialReactTable table={table} />
//       </Box>
//     </>
//   );
// }
