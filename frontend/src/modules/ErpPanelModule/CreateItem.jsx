import { useState, useEffect } from 'react';

import { Button, Tag, Form, Divider } from 'antd';
import { PageHeader } from '@ant-design/pro-layout';

import { useSelector, useDispatch } from 'react-redux';

import useLanguage from '@/locale/useLanguage';

import { settingsAction } from '@/redux/settings/actions';
import { currencyAction } from '@/redux/currency/actions';
import { erp } from '@/redux/erp/actions';
import { selectCreatedItem } from '@/redux/erp/selectors';
import { selectCompanySettings } from '@/redux/settings/selectors';
import {selectCompanyCode} from '@/redux/company/selectors'

import calculate from '@/utils/calculate';
import { generate as uniqueId } from 'shortid';

import Loading from '@/components/Loading';
import { ArrowLeftOutlined, ArrowRightOutlined, CloseCircleOutlined, PlusOutlined ,EyeOutlined, AuditOutlined, DollarOutlined} from '@ant-design/icons';

import { useNavigate } from 'react-router-dom';
import { selectLangDirection } from '@/redux/translate/selectors';

function SaveForm({ form }) {
  const translate = useLanguage();
  const handelClick = () => {
    form.submit();
  };

  return (
    <Button onClick={handelClick} type="primary" icon={<PlusOutlined />}>
      {translate('Save')}
    </Button>
  );
}

export default function CreateItem({ config, CreateForm }) {
  const translate = useLanguage();
  const dispatch = useDispatch();
  const navigate = useNavigate();


  useEffect(() => {
    dispatch(settingsAction.list({ entity: 'setting' }));
    dispatch(currencyAction.list());
  }, []);
  
  let { entity } = config;

  const { isLoading, isSuccess, result } = useSelector(selectCreatedItem);
  const company=useSelector(selectCompanyCode);

  const [form] = Form.useForm();
  const [subTotal, setSubTotal] = useState(0);
  const [offerSubTotal, setOfferSubTotal] = useState(0);
  const [prices, setPrices] = useState([]); // Estado para guardar los precios



  const handleInputChange = (index, data) => {
    if(data.price && data.description) {
      const itemsValues = form.getFieldValue('items');

      if (itemsValues && itemsValues[index]) {
        itemsValues[index].price = data.price;
        itemsValues[index].description = data.index;
      }
      // Establecer los nuevos valores en el formulario
      form.setFieldsValue({ items: itemsValues });

    }  
  };


  const handelValuesChange = (changedValues, values) => {
    
    const items = values['items'];
    let subTotal = 0;
    let subOfferTotal = 0;

    if (items) {

      items.map((item,index) => {
        if (item) {

          if (item.offerPrice && item.quantity) {
            let offerTotal = calculate.multiply(item['quantity'], item['offerPrice']);
            subOfferTotal = calculate.add(subOfferTotal, offerTotal);
          }

          if (item.quantity && item.price) {
            let total = calculate.multiply(item['quantity'], item['price']);
            //sub total
            subTotal = calculate.add(subTotal, total);
          }
        }
      });
      setSubTotal(subTotal);
      setOfferSubTotal(subOfferTotal);
    }
  };

  useEffect(() => {
    if (isSuccess) {
      form.resetFields();
      dispatch(erp.resetAction({ actionType: 'create' }));
      setSubTotal(0);
      setOfferSubTotal(0);
      navigate(`/${entity.toLowerCase()}/read/${result._id}`);
    }
    return () => {};
  }, [isSuccess]);

  const onSubmit = (fieldsValue) => {
    console.log('🚀 ~ onSubmit ~ fieldsValue:', fieldsValue);
    if (fieldsValue) {
      if (fieldsValue.items) {
        let newList = [...fieldsValue.items];
        newList.map((item) => {
          item.total = calculate.multiply(item.quantity, item.price);
        });
        fieldsValue = {
          ...fieldsValue,
          items: newList,
        };
      }
    }
    dispatch(erp.create({ entity, jsonData: fieldsValue }));
  };

const langDirection=useSelector(selectLangDirection)
  return (
    <>
      <PageHeader
        onBack={() => {
          navigate(`/${entity.toLowerCase()}`);
        }}
        backIcon={langDirection==="rtl"?<ArrowRightOutlined/>:<ArrowLeftOutlined />}

        title={<span style={{ color: '#F0EBE3' }}>{translate('New'+' '+entity)}</span>}
        ghost={false}
        tags={<Tag>{translate('Draft')}</Tag>}
        // subTitle="This is create page"
        
        extra={[
          <Button
          danger
            key={`${uniqueId()}`}
            onClick={() => navigate(`/${entity.toLowerCase()}`)}
            icon={<CloseCircleOutlined />}
          >
            {translate('Cancel')}
          </Button>,
          <Button
          danger
          key={`${uniqueId()}`}
          onClick={() => {
            navigate(`/${entity.toLowerCase()}/read/${current?._id}`);
          }}
          icon={<EyeOutlined />}
          >
          {translate('view info')}
          </Button>,
          <Button
          danger
          key={`${uniqueId()}`}
          onClick={() => {
            navigate(`/${entity.toLowerCase()}/read/${current?._id}`);
          }}
          icon={<DollarOutlined />}
          >
          {translate('Cobrar')}
          </Button>,
          <Button
          danger
          key={`${uniqueId()}`}
          onClick={() => {
            navigate(`/${entity.toLowerCase()}/read/${current?._id}`);
          }}
          icon={<AuditOutlined />}
          >
          {translate('Enbarcar')}
          </Button>,
          <SaveForm form={form} key={`${uniqueId()}`} />,
        ]}
        style={{
          padding: '20px 15px',
          background: company ==='EB'?'#E64848': '#EF9C66'
        }}
      ></PageHeader>
      <Loading isLoading={isLoading}>

        <Form form={form} layout="vertical" onFinish={onSubmit} onValuesChange={handelValuesChange}>
          <CreateForm subTotal={subTotal} offerTotal={offerSubTotal} handleInputChange={handleInputChange} />
        </Form>
      </Loading>
    </>
  );
}
