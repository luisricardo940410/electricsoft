import { useState, useEffect } from 'react';
import { Form, Divider } from 'antd';
import dayjs from 'dayjs';
import { Button, Tag } from 'antd';
import { PageHeader } from '@ant-design/pro-layout';

import { useSelector, useDispatch } from 'react-redux';
import useLanguage from '@/locale/useLanguage';
import { erp } from '@/redux/erp/actions';

import calculate from '@/utils/calculate';
import { generate as uniqueId } from 'shortid';
import { selectUpdatedItem } from '@/redux/erp/selectors';
import {selectCompanyCode} from '@/redux/company/selectors'
import Loading from '@/components/Loading';
import { tagColor } from '@/utils/statusTagColor';

import { CloseCircleOutlined, PlusOutlined, EyeOutlined } from '@ant-design/icons';
import { useNavigate, useParams } from 'react-router-dom';

import { settingsAction } from '@/redux/settings/actions';
// import { StatusTag } from '@/components/Tag';

function SaveForm({ form, translate }) {
  const handelClick = () => {
    form.submit();
  };

  return (
    <Button onClick={handelClick} type="primary" icon={<PlusOutlined />}>
      {translate('update')}
    </Button>
  );
}

export default function UpdateItem({ config, UpdateForm }) {
  const translate = useLanguage();
  let { entity } = config;

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { current, isLoading, isSuccess } = useSelector(selectUpdatedItem);
  const [form] = Form.useForm();
  const [subTotal, setSubTotal] = useState(0);

  const resetErp = {
    status: '',
    client: {
      name: '',
      email: '',
      phone: '',
      address: '',
    },
    subTotal: 0,
    taxTotal: 0,
    taxRate: 0,
    total: 0,
    credit: 0,
    number: 0,
    year: 0,
  };

  const [currentErp, setCurrentErp] = useState(current ?? resetErp);
  const company=useSelector(selectCompanyCode);

  const { id } = useParams();


  
  const handleInputChange = (index, data) => {

    if(data.price && data.description) {
      const itemsValues = form.getFieldValue('items');

      if (itemsValues && itemsValues[index]) {
        itemsValues[index].price = data.price;
        itemsValues[index].description = data.index;

      }
      // Establecer los nuevos valores en el formulario
      form.setFieldsValue({ items: itemsValues });

    }  
  };

  const handelValuesChange = (changedValues, values) => {
    const items = values['items'];
    let subTotal = 0;

    if (items) {
      items.map((item) => {
        if (item) {
          if (item.quantity && item.price) {
            let total = calculate.multiply(item['quantity'], item['price']);
            subTotal = calculate.add(subTotal, total);
          }
        }
      });
      setSubTotal(subTotal);
    }
  };

  const onSubmit = (fieldsValue) => {
    console.log('Valores a update: ',fieldsValue)
    let dataToUpdate = { ...fieldsValue };
    if (fieldsValue) {
      
      if (fieldsValue.date || fieldsValue.expiredDate) {
        dataToUpdate.date = dayjs(fieldsValue.date).format('YYYY-MM-DDTHH:mm:ss.SSSZ');
        dataToUpdate.expiredDate = dayjs(fieldsValue.expiredDate).format(
          'YYYY-MM-DDTHH:mm:ss.SSSZ'
        );
      }
      if (fieldsValue.items) {
        let newList = [];
        fieldsValue.items.map((item) => {
          const { quantity, price, product, description } = item;
          const total = item.quantity * item.price;
          newList.push({ total, quantity, price, product, description });
        });
        dataToUpdate.items = newList;
      }
      if (fieldsValue.file) {
        dataToUpdate.file  = fieldsValue.file[0].originFileObj;
      }
    }

    dispatch(erp.updateAndLoad({ entity, id, jsonData: dataToUpdate }));
  };

  useEffect(() => {
    if (isSuccess) {
      form.resetFields();
      setSubTotal(0);
      dispatch(erp.resetAction({ actionType: 'update' }));
      navigate(`/${entity.toLowerCase()}/read/${id}`);
    }
  }, [isSuccess]);

  const updateCurrency = (value) => {
    dispatch(
      settingsAction.updateCurrency({
        data: { default_currency_code: value },
      })
    );
  };

  useEffect(() => {
    if (current) {
      setCurrentErp(current);
      let formData = { ...current };
      if (formData.date) {
        formData.date = dayjs(formData.date);
      }
      if (formData.expiredDate) {
        formData.expiredDate = dayjs(formData.expiredDate);
      }
      if (!formData.taxRate) {
        formData.taxRate = 0;
      }

      updateCurrency(current.currency);

      const { subTotal } = formData;

      form.resetFields();
      form.setFieldsValue(formData);
      setSubTotal(subTotal);
    }
  }, [current]);

  return (
    <>
      <PageHeader
        onBack={() => {
          navigate(`/${entity.toLowerCase()}`);
        }}
        title={<span style={{ color: '#F0EBE3' }}>{translate('update')+' '+translate(entity)}</span>}
        ghost={false}
        tags={[
          currentErp.status && (
            <Tag color={tagColor(currentErp.status)?.color} key="status">
            {currentErp.status && translate(currentErp.status)}
            </Tag>
          ),
          currentErp.paymentStatus && (
            <Tag color={tagColor(currentErp.paymentStatus)?.color} key="paymentStatus">
              {currentErp.paymentStatus && translate(currentErp.paymentStatus)}
            </Tag>
          ),
          current?.enabled && (
            <Tag color={currentErp?.enabled ? 'green' : 'gray'} key="status">
            {currentErp.enabled && translate('Active')}
          </Tag>
          )

        ]}
        extra={[
          <Button
            key={`${uniqueId()}`}
            onClick={() => {
              navigate(`/${entity.toLowerCase()}`);
            }}
            icon={<CloseCircleOutlined />}
          >
            {translate('Cancel')}
          </Button>,
          <Button
          key={`${uniqueId()}`}
          onClick={() => {
            navigate(`/${entity.toLowerCase()}/read/${current?._id}`);
          }}
          icon={<EyeOutlined />}
          >
          {translate('view info')}
          </Button>,
          <SaveForm translate={translate} form={form} key={`${uniqueId()}`} />,
        ]}
        style={{
          padding: '20px 15px',
          background: company ==='EB'?'#E64848': '#EF9C66'
        }}
      ></PageHeader>
      <Divider dashed />
      <Loading isLoading={isLoading}>
        <Form form={form} layout="vertical" onFinish={onSubmit} onValuesChange={handelValuesChange}>
          <UpdateForm subTotal={subTotal} current={current} handleInputChange={handleInputChange}/>
        </Form>
      </Loading>
    </>
  );
}
