import { ErpLayout } from '@/layout';
import CreateItem from '@/modules/ErpPanelModule/CreateItem';
import SaleForm from '@/modules/SaleModule/Forms/SaleForm';

export default function CreateSaleModule({ config }) {
  return (
    <ErpLayout>
      <CreateItem config={config} CreateForm={SaleForm} />
    </ErpLayout>
  );
}
