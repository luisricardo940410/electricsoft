import { useDispatch, useSelector } from 'react-redux';
import { Input, InputNumber, Form, Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

import languages from '@/locale/languages';
import useLanguage from '@/locale/useLanguage';
import { translateAction } from '@/redux/translate/actions';
import { countryList } from '@/utils/countryList';
import { selectLangDirection } from '@/redux/translate/selectors';

export default function EmailSettingForm() {
  const translate = useLanguage();
  const dispatch = useDispatch();
  const langDirection = useSelector(selectLangDirection)
  return (
    <div style={{ direction: langDirection }}>
      <Form.Item
        label={translate('email')}
        name="electricsoft_app_company_email"
        rules={[
          {
            required: true,
            type: 'email',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label={translate('Port')}
        name="electricsoft_app_port_email"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <InputNumber min={0} />
      </Form.Item>
      <Form.Item
        label={translate('secure')}
        name={'electricsoft_app_secure_email'}
        rules={[
          {
            required: false,
            type: 'boolean',
          },
        ]}
        valuePropName={'checked'}
      >
        <Switch checkedChildren={<CheckOutlined/>} unCheckedChildren={<CloseOutlined/>}/>
      </Form.Item>
      <Form.Item
        label={translate('User')}
        name="electricsoft_app_user_email"
        rules={[
          {
            required: true,
            message: 'Please input your username!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={translate('Password')}
        name="electricsoft_app_password_email"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>


    </div>
  );
}
