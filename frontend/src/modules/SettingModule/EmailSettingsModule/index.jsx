import SetingsSection from '../components/SetingsSection';
import UpdateSettingModule from '../components/UpdateSettingModule';
import EmailSettingForm from './forms/EmailSettingForm';
import useLanguage from '@/locale/useLanguage';

export default function EmailSettingsModule({ config }) {
  const translate = useLanguage();
  return (
    <UpdateSettingModule config={config}>
      <SetingsSection
        title={translate('App Settings')}
        description={translate('Update your app email configuration')}
      >
        <EmailSettingForm />
      </SetingsSection>
    </UpdateSettingModule>
  );
}
