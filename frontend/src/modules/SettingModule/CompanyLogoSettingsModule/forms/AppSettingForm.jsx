import { Button, Form, message, Upload, Avatar, Image, Row, Col } from 'antd';
import { useSelector } from 'react-redux';

import { UploadOutlined } from '@ant-design/icons';
import { FILE_BASE_URL } from '@/config/serverApiConfig';

import useLanguage from '@/locale/useLanguage';
import { selectCompanySettings } from '@/redux/settings/selectors';


export default function AppSettingForm() {
  const currentSettings = useSelector(selectCompanySettings);
  const translate = useLanguage();
  const beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 5;
    if (!isLt2M) {
      message.error('Image must smaller than 5MB!');
    }
    return false;
  };
  return (
    <>
    {/* <Row align="middle">
      <Col xs={{ span: 24 }} sm={{ span: 7 }} md={{ span: 5 }}>
      <Image
          className="last rigth"
          src={currentSettings?.company_logo ? FILE_BASE_URL + currentSettings?.company_logo : undefined}
          size={96}
          style={{
            color: '#f56a00',
            backgroundColor: currentSettings?.company_logo ? 'none' : '#fde3cf',
            boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 6px 1px',
          }}
          alt={`${currentSettings?.company_name}`}
        >
          {currentSettings?.company_name.charAt(0).toUpperCase()}
        </Image>
      </Col>
      <Col xs={{ span: 24 }} sm={{ span: 18 }}>
      <Form.Item
          name="file"
          label="Cargar Logo"
          valuePropName="fileList"
          getValueFromEvent={(e) => e.fileList}
        >
          <Upload
            beforeUpload={beforeUpload}
            listType="picture"
            accept="image/png, image/jpeg"
            maxCount={1}
          >
            <Button icon={<UploadOutlined />}>{translate('click_to_upload')}</Button>
          </Upload>
        </Form.Item>
      </Col>
    </Row> */}

<Row align="middle" gutter={[24, 24]}>
      <Col xs={24} sm={10} md={8}>
        <Image
          className="last rigth"
          src={currentSettings?.company_logo ? FILE_BASE_URL + currentSettings?.company_logo : undefined}
          width={170}
          style={{
            color: '#f56a00',
            backgroundColor: currentSettings?.company_logo ? 'none' : '#fde3cf',
            boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 6px 1px',
          }}
          alt={currentSettings?.company_name}
        />
      </Col>
      <Col xs={24} sm={10} md={10}>
        <Form.Item
          name="file"
          label="upload logo"
          valuePropName="fileList"
          getValueFromEvent={(e) => e.fileList}
        >
          <Upload
            beforeUpload={beforeUpload}
            listType="picture"
            accept="image/png, image/jpeg"
            maxCount={1}
          >
            <Button icon={<UploadOutlined />}>{translate('click_to_upload')}</Button>
          </Upload>
        </Form.Item>
      </Col>
    </Row>
    </>
  );
}
