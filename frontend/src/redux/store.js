import { configureStore } from '@reduxjs/toolkit';

import lang from '@/locale/translation/es_es';

import rootReducer from './rootReducer';
import storePersist, { localStorageHealthCheck } from './storePersist';

localStorageHealthCheck();

const LANG_INITIAL_STATE = {
  result: lang,
  langCode: 'en_us',
  isLoading: false,
  isSuccess: false,
};

const lang_state = storePersist.get('translate') ? storePersist.get('translate') : LANG_INITIAL_STATE;

const AUTH_INITIAL_STATE = {
  current: {},
  isLoggedIn: false,
  isLoading: false,
  isSuccess: false,
};

const auth_state = storePersist.get('auth') ? storePersist.get('auth') : AUTH_INITIAL_STATE;

const COMPANY_INITIAL_STATE = {
  result: {},
  companyId: '665dfba05e037b49390368c8',
  companyName: 'Eléctrica Bugambilias',
  companyCode: 'EB',
  isLoading: false,
  isSuccess: false,
};
const company_state = storePersist.get('company') ? storePersist.get('company') : COMPANY_INITIAL_STATE;

const initialState = { translate: lang_state, auth: auth_state, company: company_state };

const store = configureStore({
  reducer: rootReducer,
  preloadedState: initialState,
  devTools: import.meta.env.PROD === false, // Enable Redux DevTools in development mode
});

//  console.log(
//    '🚀 Welcome to ELECTRICSOFT ERP! Did you know that we also offer commercial customization services? Contact us at hello@idurarapp.com for more information.'
//  );

export default store;
