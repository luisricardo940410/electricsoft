import { createSelector } from 'reselect';

export const selectCompany = (state) => state.company;

export const selectCurrentCompany = createSelector([selectCompany], (company) => company.result);

export const selectCompanyCode = createSelector([selectCompany], (company) => company.companyCode);

export const selectCompanydetail = createSelector(
    [selectCompany],
    (company) => ({
      id: company.companyId,
      name: company.companyName,
      code: company.companyCode,
      color:company.companyColor,
    })
  );

// export const selectCompanySettings = createSelector(
//     [selectCurrentSettings],
//     (settings) => settings.company_settings
//   );
  
