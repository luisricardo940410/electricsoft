import * as actionTypes from './types';
import { request } from '@/request';

export const companyAction = {
  resetState: () => (dispatch) => {
    dispatch({
      type: actionTypes.RESET_STATE,
    });
  },
  changeCompany:
    ( {data} ) =>
    async (dispatch) => {
      dispatch({
        type: actionTypes.REQUEST_LOADING,
      });
      let companies = await request.listAll({ entity: 'company', options: { enabled: true } });

      if (companies) {
        // NOTE: the component SelectCompany send the var data
        const COMPANY_STATE = {
          result: companies.result,
          companyId:  data.default_id,
          companyName: data.default_name,
          companyCode: data.default_company_code,
          companyColor: data.default_company_color,
          isLoading: false,
          isSuccess: false,
        };
        window.localStorage.setItem('company', JSON.stringify(COMPANY_STATE));
        dispatch({
          type: actionTypes.REQUEST_SUCCESS,
          payload:  companies.result,
          companyId:  data.default_id,
          companyName: data.default_name,
          companyCode: data.default_company_code,
          companyColor: data.default_company_color,
        });
      } else {
        dispatch({
          type: actionTypes.REQUEST_FAILED,
        });
      }
    },
};
