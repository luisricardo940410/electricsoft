import * as actionTypes from './types';

const INITIAL_STATE = {
  result: [],
  companyId: '665dfba05e037b49390368c8',
  companyName: 'Eléctrica Bugambilias',
  companyCode: 'EB',
  companyColor: '#FFFFFF',
  isLoading: false,
  isSuccess: false,
};

const companyReducer = (state = INITIAL_STATE, action) => {
 
  const { payload = null , companyId , companyName , companyCode, companyColor} = action;  
  switch (action.type) {
    case actionTypes.RESET_STATE:
      return INITIAL_STATE;
      
    case actionTypes.REQUEST_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case actionTypes.REQUEST_FAILED:
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
      };

    case actionTypes.REQUEST_SUCCESS:
      return {
        result: payload,
        companyId: companyId,
        companyName: companyName,
        companyCode: companyCode,
        companyColor: companyColor,
        isLoading: false,
        isSuccess: true,
      };
    default:
      return state;
  }
};

export default companyReducer;
