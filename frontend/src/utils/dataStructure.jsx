import { useMemo } from 'react';
import dayjs from 'dayjs';
import { Switch, Tag, Avatar, Image } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { countryList } from '@/utils/countryList';
import { generate as uniqueId } from 'shortid';
import color from '@/utils/color';
import { FILE_BASE_URL } from '@/config/serverApiConfig';

export const dataForRead = ({ fields, translate }) => {
  let columns = [];

  Object.keys(fields).forEach((key) => {
    let field = fields[key];
      columns.push({
        header: field.label ? field.label : key,
        accessorKey: field.dataIndex ? field.dataIndex.join('.') : key,
        isDate: field.type === 'date',
      });
  
  });

  return columns;
};


export function dataForTable({ fields, translate, moneyFormatter, dateFormat }) {
  const columns = useMemo(() => {
    let cols = [];

    Object.keys(fields).forEach((key) => {
      let field = fields[key];
      
      const keyIndex = field.dataIndex ? field.dataIndex : key;

      const component = {
        boolean: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ row }) => (
            <Switch
              checked={row.original[key]}
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
            />
          ),
        },
        date: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell }) => {
            const date = dayjs(cell.value).format(dateFormat);
            return <Tag label={date} style={{ backgroundColor: field.color }} />;
          },
        },
        currency: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell, row }) => moneyFormatter({ amount: row.original[key], currency_code: row.original.currency }),
        },
        async: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ renderedCellValue, row }) => (
            <Tag bordered={false} color={field.color || row.original[key]?.color || row.color}>
              {console.log('datos asyc',row.original[key]?.description)}
                    {row.original[key]?.description}
            </Tag>
          ),
        },
        color: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell,row }) => {
            const colorLabel = color.find((x) => x.value === row.original[key])?.label;
            return (
              // <Tag label={colorLabel} style={{ backgroundColor: cell.value }} />
              <Tag bordered={false} color={field.color || row.original[key]?.color || row.original.color}>
              {colorLabel}
            </Tag>

            )
          },
        },
        stringWithColor: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ renderedCellValue, row }) => (
            // <Tag label={row.or} style={{ backgroundColor: field.color || row.original[key]?.color || row.original.color }} />
            <Tag color={field.color || row.original[key]?.color || row.original.color}>
              {renderedCellValue}
            </Tag>
          ),
        },
        tag: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell }) => <Tag label={cell.value} style={{ backgroundColor: field.color }} />,
        },
        selectWithFeedback: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell, row }) => {
            if (field.renderAsTag) {
              const selectedOption = field.options.find((x) => x.value === row.original[key]);
              return (
              <Tag bordered={true} color={ selectedOption?.color}>
                {row.original[key] && translate(row.original[key])}
              </Tag>
              );
            } else {
              return translate(row.original[key]);
            }
          },
        },
        select: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell, row }) => {
            console.log('select', field)
            if (field.renderAsTag) {
              const selectedOption = field.options.find((x) => x.value === row.original[key]);
              return (
              <Tag color={row.original?.color || field?.color || selectedOption?.color}>
                {row.original[key]} 
              </Tag>

              )
            } else {
              return row.original[key];
            }
          },
        },
        selectWithTranslation: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({row}) => {
            
            if (field.renderAsTag) {
              const selectedOption = field.options.find((x) => x.value === row.original[key]);
              return (
                <Tag color={ field?.color || selectedOption?.color } >
                  {translate(row.original[key])} 
                </Tag>
              );
            } else {
              return translate(row.original[key]);
            }
          },
        },
        array: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell }) => {
            return cell.value.map((x) => (
              <Tag key={uniqueId()} label={x} style={{ backgroundColor: field.colors[x] }} />
            ));
          },
        },
        country: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({ cell,row }) => {
           

            const selectedCountry = countryList.find((obj) => obj.value === row.original[key]);
            console.log(row.original[key])
            return (
              <Tag bordered={true} color={field?.color || row.original?.color }>
              {`${selectedCountry?.icon ? selectedCountry.icon + ' ' : ''}${selectedCountry?.label ? translate(selectedCountry.label) : ''}`}
              </Tag>
            );
          },
        },
        image: {
          header: field.label ? translate(field.label) : translate(key),
          accessorKey: keyIndex,
          Cell: ({cell, row}) =>(
            <div style={{ display: 'flex', alignItems: 'center' }}>
            <Avatar
              className="last"
              src={
                row.original[key]
                ?
                <Image
                  src={row.original[key] ? FILE_BASE_URL + row.original[key] : undefined}
                  width={40}
                  height={40}
                  preview={true} // Disable preview
                />
                :
                undefined
              }
              style={{
                color: '#f56a00',
                backgroundColor: row.original[key] ? 'none' : '#fde3cf',
                boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 10px 2px',
                cursor: 'pointer',
                marginRight: '8px', // space between avatar and name
              }}
              size="large"
            >
              {row.original?.name?.charAt(0)?.toUpperCase()}
            </Avatar>
            <span>{row.original?.storeCode}</span>
          </div>
          )
 
        },
      };

      const defaultComponent = {
        header: field.label ? translate(field.label) : translate(key),
        accessorKey: keyIndex,
      };

      const type = field.type;

      if (!field.disableForTable) {
        if (Object.keys(component).includes(type)) {
          cols.push(component[type]);
        } else {
          cols.push(defaultComponent);
        }
      }
    });

    return cols;
  }, [fields, translate, moneyFormatter, dateFormat, countryList]);

  return columns;
}

function getRandomColor() {
  const colors = [
    'magenta',
    'red',
    'volcano',
    'orange',
    'gold',
    'lime',
    'green',
    'cyan',
    'blue',
    'geekblue',
    'purple',
  ];

  // Generate a random index between 0 and the length of the colors array
  const randomIndex = Math.floor(Math.random() * colors.length);

  // Return the color at the randomly generated index
  return colors[randomIndex];
}
