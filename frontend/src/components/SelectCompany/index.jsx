import { useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { companyAction } from '@/redux/company/actions';

import { translateAction } from '@/redux/translate/actions';
import useLanguage from '@/locale/useLanguage';
import { request } from '@/request';
import { useNavigate } from 'react-router-dom';
import { selectCompanyCode } from '@/redux/company/selectors';


import { Select } from 'antd';
import useResponsive from '@/hooks/useResponsive';
import useFetch from '@/hooks/useFetch';

 const SelectCompany = () => {
  const dispatch = useDispatch();
  const translate = useLanguage();

  const { isMobile } = useResponsive();
  const [selectOptions, setOptions] = useState([]);

  const navigate = useNavigate();

  const asyncList = () => {
    return request.listAll({ entity: 'company', options: { enabled: true } });
  };

  const { result, isLoading: fetchIsLoading, isSuccess } = useFetch(asyncList);
 
  useEffect(() => {
    if (isSuccess) {
      setOptions(result);
    }
  }, [isSuccess]);

  const CurrentCompany = useSelector(selectCompanyCode);

  const updateCompany= (value) => {
    const company = selectOptions.find((x) => x.company_code === value);
    // console.log('selectCompany',company)
    dispatch(
      companyAction.changeCompany({
        data: { default_id: company._id, default_name: company.name , default_company_code: company.company_code, default_company_color:company.color},
      })
    );
  };



  const handleSelectChange = (newValue) => {
    if (newValue === 'redirectURL') {
      navigate('/settings/currency');
    }
  };

  const optionsList = () => {
    const list = [];

    const value = 'redirectURL';
    const label = `+ Add New Company`;

    // // Iterating over selectOptions to add company names to the list
    selectOptions.forEach(option => {
      list.push({ 
        key:option.company_code,
        value: option.company_code, 
        label: option.name,
        disabled: option.disabled
      });
    });
    return list;


  };


   // Finding the current company object to set as the value
    const currentCompanyOption = selectOptions.find(option => option.company_code === CurrentCompany);


    return (
      <Select
        showSearch
        placeholder={translate('select company')}
        // value={CurrentCompany}
        value={currentCompanyOption ? { value: currentCompanyOption.company_code, label: currentCompanyOption.name } : null}
        loading={fetchIsLoading}
        defaultOpen={false}
        optionFilterProp="children"
        filterOption={(input, option) => (option?.label ?? '').includes(input.toLowerCase())}
        filterSort={(optionA, optionB) =>
          (optionA?.label ?? '').toLowerCase().startsWith((optionB?.label ?? '').toLowerCase())
        }
        options={optionsList()}
        onSelect={updateCompany}
        onChange={handleSelectChange}
        style={{
          width: isMobile ? '90px' : '250px',
          float: 'left',
          marginTop: '5px',
          // marginRight: '200px', // Add the marginRight property here
          cursor: 'pointer',
          direction: 'ltr',
        }}
      ></Select>
    );
};

export default SelectCompany;
