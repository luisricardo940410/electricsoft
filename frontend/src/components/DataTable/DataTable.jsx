import { useCallback, useEffect } from 'react';

import {
  EyeOutlined,
  EditOutlined,
  DeleteOutlined,
  RedoOutlined,
  ArrowRightOutlined,
  ArrowLeftOutlined,
} from '@ant-design/icons';

//material components
import {MaterialReactTable,useMaterialReactTable} from 'material-react-table';
import { Box, IconButton, Tooltip, MenuItem } from '@mui/material';
import { Edit as EditIcon, Delete as DeleteIcon, RemoveRedEye as EyeIcon} from '@mui/icons-material';

import { Dropdown, Table, Button, Input } from 'antd';
import { PageHeader } from '@ant-design/pro-layout';

import { useSelector, useDispatch } from 'react-redux';
import { crud } from '@/redux/crud/actions';
import { selectListItems } from '@/redux/crud/selectors';
import useLanguage from '@/locale/useLanguage';
import { dataForTable } from '@/utils/dataStructure';
import { useMoney, useDate } from '@/settings';

import { generate as uniqueId } from 'shortid';

import { useCrudContext } from '@/context/crud';
import { selectLangDirection } from '@/redux/translate/selectors';
import { selectCompanyCode } from '@/redux/company/selectors';

function AddNewItem({ config }) {
  const { crudContextAction } = useCrudContext();
  const { collapsedBox, panel } = crudContextAction;
  const { ADD_NEW_ENTITY } = config;

  const handelClick = () => {
    panel.open();
    collapsedBox.close();
  };

  return (
    <Button onClick={handelClick} type="primary">
      {ADD_NEW_ENTITY}
    </Button>
  );
}
export default function DataTable({ config, extra = [] }) {
  let { entity, dataTableColumns, DATATABLE_TITLE, fields, searchConfig } = config;
  const { crudContextAction } = useCrudContext();
  const { panel, collapsedBox, modal, readBox, editBox, advancedBox } = crudContextAction;
  const translate = useLanguage();
  const { moneyFormatter } = useMoney();
  const { dateFormat } = useDate();
  const company = useSelector(selectCompanyCode)

  const items = [
    {
      label: 'Show',
      key: 'read',
      icon: <EyeOutlined />,
      color: 'secondary',
    },
    {
      label: 'Edit',
      key: 'edit',
      icon: <EditOutlined />,
      color: 'primary',
    },
    {
      label: 'Delete',
      key: 'delete',
      icon: <DeleteOutlined />,
      color: 'error',
    },
    // Add any extra items here
    ...extra,
  ];

  const handleRead = (record) => {
    dispatch(crud.currentItem({ data: record }));
    panel.open();
    collapsedBox.open();
    readBox.open();
  };
  function handleEdit(record) {
    dispatch(crud.currentItem({ data: record }));
    dispatch(crud.currentAction({ actionType: 'update', data: record }));
    editBox.open();
    panel.open();
    collapsedBox.open();
  }
  function handleDelete(record) {
    dispatch(crud.currentAction({ actionType: 'delete', data: record }));
    modal.open();
  }

  function handleUpdatePassword(record) {
    dispatch(crud.currentItem({ data: record }));
    dispatch(crud.currentAction({ actionType: 'update', data: record }));
    advancedBox.open();
    panel.open();
    collapsedBox.open();
  }

  let dispatchColumns = [];
  if (fields) {
    dispatchColumns = [...dataForTable({ fields, translate, moneyFormatter, dateFormat })];
  } else {
    dispatchColumns = [...dataTableColumns];
  }

  dataTableColumns = [
    ...dispatchColumns,
    {
      header: 'Action',
      key: 'mrt-row-actions',
      Cell: ({ cell, row }) => (
      <Box>
      {items.map((item) => (
        <Tooltip key={item.key} title={item.label}>
          <IconButton color={item.color} onClick={() => {
             switch (item.key) {
                case 'read':
                  handleRead(row.original);
                  break;
                case 'edit':
                  handleEdit(row.original);
                  break;

                case 'delete':
                  handleDelete(row.original);
                  break;
                case 'updatePassword':
                  handleUpdatePassword(row.original);
                  break;
                default:
                  break;
            }
          }}>
            {item.icon}
          </IconButton>
        </Tooltip>
      ))}
      </Box>
      ),
    },
  ];

  const { result: listResult, isLoading: listIsLoading } = useSelector(selectListItems);

  const { pagination, items: dataSource } = listResult;

  const dispatch = useDispatch();

  const handelDataTableLoad = useCallback((pagination) => {
    const options = { page: pagination.current || 1, items: pagination.pageSize || 10 };
    dispatch(crud.list({ entity, options }));
  }, []);

  const filterTable = (e) => {
    const value = e.target.value;
    const options = { q: value, fields: searchConfig?.searchFields || '' };
    dispatch(crud.list({ entity, options }));
  };

  const dispatcher = () => {
    dispatch(crud.list({ entity }));
  };

  useEffect(() => {
    const controller = new AbortController();
    dispatcher();
    return () => {
      controller.abort();
    };
  }, []);

  const langDirection=useSelector(selectLangDirection)

  const table = useMaterialReactTable({
    columns:dataTableColumns,
    data:dataSource,
    enableGlobalFilter:false,
    initialState: { 
      enableGlobalFilterModes:false,
      showGlobalFilter: false,
      showColumnFilters: false,
      pagination: pagination,
      density:'compact'
    },
    state: {
      isLoading:listIsLoading,
    },
    paginationDisplayMode: 'pages',
    positionToolbarAlertBanner: 'bottom',
    muiPaginationProps: {
      color: 'primary',
      rowsPerPageOptions: [10, 100, 500],
      shape: 'rounded',
      variant: 'outlined',
    },
  });

  return (
    <>
      <PageHeader
        onBack={() => window.history.back()}
        backIcon={langDirection==="rtl"?<ArrowRightOutlined/>:<ArrowLeftOutlined />}
        title={DATATABLE_TITLE}
        ghost={false}
        extra={[
          <Input
            key={`{searchFilterDataTable}`}
            onChange={filterTable}
            placeholder={translate('search')}
            allowClear
          />,
          <Button onClick={handelDataTableLoad} key={`${uniqueId()}`} icon={<RedoOutlined />}>
            {translate('Refresh')}
          </Button>,

          <AddNewItem key={`${uniqueId()}`} config={config} />,
        ]}
        style={{
          padding: '20px 15px',
          direction:langDirection,
          background: company ==='EB'?'#E64848': '#F1C93B' // Cambia este valor al color que desees rojo : FF8080 naranja:F1C93B
        }}
      ></PageHeader>

      <Box
      sx={{
        padding: '20px', // Ajusta el valor de padding según tus necesidades
      }}
    >
      <MaterialReactTable table={table} />
    </Box>
    </>
  );
}
