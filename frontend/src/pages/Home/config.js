import color from '@/utils/color';
//    disableForTable:true don't show on table
export const fields = {
  name: {
    type: 'stringWithColor',
    required: true,
  },
  description: {
    type: 'textarea',
    required: true,
  },
  enabled: {
    type: 'boolean',
    required: true,
  },
};
