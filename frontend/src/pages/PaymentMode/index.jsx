import React from 'react';

import useLanguage from '@/locale/useLanguage';

import { Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import CrudModule from '@/modules/CrudModule/CrudModule';
import PaymentModeForm from '@/forms/PaymentModeForm';

export default function PaymentMode() {
  const translate = useLanguage();
  const entity = 'paymentMode';
  const searchConfig = {
    displayLabels: ['name'],
    searchFields: 'name',
    outputValue: '_id',
  };

  const deleteModalLabels = ['name'];

  const readColumns = [
    {
      header: translate('Payment Mode'),
      accessorKey: 'name',
    },
    {
      header: translate('Description'),
      accessorKey: 'description',
    },
    {
      header: translate('Default'),
      accessorKey: 'isDefault',
    },
    {
      header: translate('enabled'),
      accessorKey: 'enabled',
    },
  ];
  const dataTableColumns = [
    {
      header: translate('Payment Mode'),
      accessorKey: 'name',
    },
    {
      header: translate('Description'),
      accessorKey: 'description',
    },
    {
      header: translate('Default'),
      accessorKey: 'isDefault',
      key: 'isDefault',
      // onCell: (record, rowIndex) => {
      //   return {
      //     props: {
      //       style: {
      //         width: '60px',
      //       },
      //     },
      //   };
      // },
      Cell: ({row}) => {
        return (
          <Switch
            checked={row.original.isDefault}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        );
      },
    },
    {
      header: translate('enabled'),
      accessorKey: 'enabled',
      key: 'enabled',
      // onCell: (record, rowIndex) => {
      //   return {
      //     props: {
      //       style: {
      //         width: '60px',
      //       },
      //     },
      //   };
      // },
      Cell: ({row}) => {
        return (
          <Switch
            checked={row.original.enabled}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        );
      },
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('payment_mode'),
    DATATABLE_TITLE: translate('payment_mode_list'),
    ADD_NEW_ENTITY: translate('add_new_payment_mode'),
    ENTITY_NAME: translate('payment_mode'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    readColumns,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return (
    <CrudModule
      createForm={<PaymentModeForm />}
      updateForm={<PaymentModeForm isUpdateForm={true} />}
      config={config}
    />
  );
}
