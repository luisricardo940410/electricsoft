import ProductDataTableModule from '@/modules/ProductModule/ProductDataTableModule';
import CrudModule from '@/modules/CrudModule/CrudModule';
import DynamicForm from '@/forms/DynamicForm';
import { getConfig } from './config';
import { useMoney, useDate } from '@/settings';

import useLanguage from '@/locale/useLanguage';

export default function Product() {
  const translate = useLanguage();
  const { moneyFormatter } = useMoney();
  const { fields, dataTableColumns } = getConfig(translate,moneyFormatter);

  const entity = 'product';
  const searchConfig = {
    entity : 'product',
    displayLabels: ['name'],
    searchFields: '_id',
  };
  const deleteModalLabels = ['description'];

  const Labels = {
    PANEL_TITLE: translate('Product'),
    DATATABLE_TITLE: translate('Product_list'),
    ADD_NEW_ENTITY: translate('add_new_Product'),
    ENTITY_NAME: translate('Product'),
  };
  
  const configPage = {
    entity,
    ...Labels,
  };

  const config = {
    ...configPage,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return (
    // <CrudModule
    //   createForm={<DynamicForm fields={fields} />}
    //   updateForm={<DynamicForm fields={fields} />}
    //   config={config}
    // />
    <ProductDataTableModule config={config}/>
  );
}
