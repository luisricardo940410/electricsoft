import { Switch, Tag, Avatar, Image} from 'antd';
import { FILE_BASE_URL } from '@/config/serverApiConfig';
import dayjs from 'dayjs';

export const getConfig = (translate,moneyFormatter) => {
  const fields = {
    name: {
      type: 'string',
      required: true,
    },
    productCategory: {
      type: 'async',
      label: 'product Category',
      displayLabels: ['productCategory', 'name'],
      accessorKey: ['productCategory', 'name'],
      entity: 'productCategory',
      required: true,
    },
    price: {
      type: 'currency',
    },
    description: {
      type: 'string',
    },
  };

  const dataTableColumns = [
    {
      header: translate('Store Code'),
      accessorKey: 'name',
      Cell: ({cell, row}) =>(
        <div style={{ display: 'flex', alignItems: 'center' }}>
        <Avatar
          className="last"
          // src={row.original?.photo ? FILE_BASE_URL + row.original?.photo : undefined}
          src={
            row.original?.photo 
            ?
            <Image
              src={row.original?.photo ? FILE_BASE_URL + row.original?.photo : undefined}
              width={40}
              height={40}
              preview={true} // Disable preview
            />
            :
            undefined
          }
          style={{
            color: '#f56a00',
            backgroundColor: row.original?.photo ? 'none' : '#fde3cf',
            boxShadow: 'rgba(150, 190, 238, 0.35) 0px 0px 10px 2px',
            cursor: 'pointer',
            marginRight: '8px', // space between avatar and name
          }}
          size="large"
        >
          {row.original?.name?.charAt(0)?.toUpperCase()}
        </Avatar>
        <span>{row.original?.storeCode}</span>
      </div>
      )
    },
    // {
    //   header: translate('name'),
    //   accessorKey: 'productCategory',
    //   Cell: ({ row }) => (
    //     <span>{row.original.name}</span>
    //   ),
    // },
    {
      header: translate('name'),
      accessorKey: 'productCategory',
      Cell: ({ row }) => (
        <span>{row.original.name}</span>
      ),
    },
    {
      header: translate('price'),
      accessorKey: 'price',
      Cell: ({ renderedCellValue,row }) => {
        // const date = dayjs(cell.value).format('YYYY-MM-DD'); // assuming 'dateFormat' was 'YYYY-MM-DD'
        // return <Tag color="green">{date}</Tag>;
         return moneyFormatter({ amount: renderedCellValue, currency_code: row.original.currency})+' '+row.original.currency
      },
    },
    {
      header: translate('quantity'),
      accessorKey: 'quantity',
      Cell: ({ renderedCellValue,row }) => {

        return (<span>{renderedCellValue+' '+row.original?.unitMeasurement.name}</span>)
      },
    },
    {
      header: translate('minimum Quantity'),
      accessorKey: 'minimumQuantity',
      Cell: ({ renderedCellValue,row }) => {

        return (<span>{renderedCellValue+' '+row.original?.unitMeasurement.name}</span>)
      },
    },
    {
      header: translate('maximum Quantity'),
      accessorKey: 'maximumQuantity',
      Cell: ({ renderedCellValue,row }) => {

        return (<span>{renderedCellValue+' '+row.original?.unitMeasurement.name}</span>)
      },
    },

  ];

  return { fields, dataTableColumns };
};
