import React from 'react';

import useLanguage from '@/locale/useLanguage';

import { Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import CrudModule from '@/modules/CrudModule/CrudModule';
import TaxForm from '@/forms/TaxForm';

export default function Taxes() {
  const translate = useLanguage();
  const entity = 'taxes';
  const searchConfig = {
    displayLabels: ['name'],
    searchFields: 'name',
    outputValue: '_id',
  };

  const deleteModalLabels = ['name'];

  const readColumns = [
    {
      header: translate('Name'),
      accessorKey: 'taxName',
    },
    {
      header: translate('Value'),
      accessorKey: 'taxValue',
    },
    {
      header: translate('Default'),
      accessorKey: 'isDefault',
    },
    {
      header: translate('enabled'),
      accessorKey: 'enabled',
    },
  ];

  const dataTableColumns = [
    {
      header: translate('Name'),
      accessorKey: 'taxName',
    },
    {
      header: translate('Value'),
      accessorKey: 'taxValue',
      // Cell: (row) => {
      //   console.log('row',row)
      //   return <>{row.original?.taxValue + '%'}</>;
      // },
    },
    {
      header: translate('Default'),
      accessorKey: 'isDefault',
      Cell: ({row}) => {
        return (
          <Switch
            checked={row.original?.isDefault}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        );
      },
    },
    {
      header: translate('enabled'),
      accessorKey: 'enabled',
      Cell: ({row}) => {
        return (
          <Switch
            checked={row.original?.enabled}
            checkedChildren={<CheckOutlined />}
            unCheckedChildren={<CloseOutlined />}
          />
        );
      },
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('taxes'),
    DATATABLE_TITLE: translate('taxes_list'),
    ADD_NEW_ENTITY: translate('add_new_tax'),
    ENTITY_NAME: translate('taxes'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    readColumns,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return (
    <CrudModule
      createForm={<TaxForm />}
      updateForm={<TaxForm isUpdateForm={true} />}
      config={config}
    />
  );
}
