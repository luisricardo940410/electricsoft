import color from '@/utils/color';

export const fields = {

  logo: {
    type: 'image',
    required:true,
    
  },
  company_code:{
    type: 'stringWithColor',
    required: true,
  },
  name: {
    type: 'string',
    required: true,
  },
  legalName: {
    type: 'string',
    required: true,
  },
  // mainContact: {
  //   type: 'search',
  //   renderAsTag: true,
  //   label: 'Contact',
  //   entity: 'people',
  //   redirectLabel: 'Add New Person',
  //   withRedirect: true,
  //   urlToRedirect: '/people',
  //   displayLabels: ['firstname', 'lastname'],
  //   searchFields: 'firstname,lastname',
  //   dataIndex: ['mainContact', 'firstname'],
  //   disableForTable:true
  // },
  country: {
    type: 'string',
  },
  phone: {
    type: 'phone',
  },
  email: {
    type: 'email',
    required: true,
  },
  website: {
    type: 'url',
  },
  // color: {
  //   type: 'color',
  //   options: [...color],
  //   required: true,
  //   disableForTable:true
  // },
  color: {
    type: 'colorPicker',
    required: true,
    disableForTable:true
  },

  // hasParentCompany: {
  //   type: 'boolean',
  //   default: false,
  // },
  // parentCompany: { type: 'search', entity: 'company' },

  // people: [{ type: 'search', entity: 'people', mutliple: true }],

  icon: {
    type: 'image',
    required:true,
    disableForTable:true
  },

  // // imageHeader: 'image',
  // bankName: {
  //   type: 'string',
  // },
  // bankIban: {
  //   type: 'string',
  // },
  // bankSwift: {
  //   type: 'string',
  // },
  // bankNumber: {
  //   type: 'string',
  // },
  // bankRouting: {
  //   type: 'string',
  // },
  // bankCountry: {
  //   type: 'string',
  // },
  // companyRegNumber: {
  //   type: 'string',
  // },
  // companyTaxNumber: {
  //   type: 'string',
  // },
  // companyTaxId: {
  //   type: 'string',
  // },
  // companyRegId: {
  //   type: 'string',
  // },
  // securitySocialNbr: 'string',
  // customField: [
  //   {
  //     fieldName: {
  //       type: 'string',

  
  //     },
  //     fieldType: {
  //       type: 'string',

  
  //       default: 'string',
  //     },
  //     fieldValue: {},
  //   },
  // ],
  location: {
    latitude: Number,
    longitude: Number,
  },
  address: {
    type: 'string',
    disableForTable:true
  },
  city: {
    type: 'string',
    disableForTable:true
  },
  State: {
    type: 'string',
    disableForTable:true
  },
  postalCode: {
    type: Number,
    disableForTable:true
  },

  // otherPhone: [
  //   {
  //     type: 'string',
  //   },
  // ],
  // fax: {
  //   type: 'string',
  // },

  // otherEmail: [
  //   {
  //     type: 'string',
  //   },
  // ],

  // socialMedia: {
  //   facebook: 'string',
  //   instagram: 'string',
  //   twitter: 'string',
  //   linkedin: 'string',
  //   tiktok: 'string',
  //   youtube: 'string',
  //   snapchat: 'string',
  // },
  // images: [
  //   {
  //     id: 'string',
  //     name: 'string',
  //     path: 'string',
  //     description: 'string',
  //     isPublic: {
  //       type: 'boolean',
  //       default: false,
  //     },
  //   },
  // ],
  // files: [
  //   {
  //     id: 'string',
  //     name: 'string',
  //     path: 'string',
  //     description: 'string',
  //     isPublic: {
  //       type: 'boolean',
  //       default: false,
  //     },
  //   },
  // ],
  // category: 'string',
  // approved: {
  //   type: 'boolean',
  //   default: true,
  // },
  // verified: {
  //   type: 'boolean',
  // },
  notes: {
    type: 'string',
    disableForTable:true
  },
};
