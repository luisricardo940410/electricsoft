import dayjs from 'dayjs';
import useLanguage from '@/locale/useLanguage';
import PaymentDataTableModule from '@/modules/PaymentModule/PaymentDataTableModule';
import { tagColor } from '@/utils/statusTagColor';
import { Tag } from 'antd';

import { useMoney, useDate } from '@/settings';

export default function Payment() {
  const translate = useLanguage();
  const { dateFormat } = useDate();
  const { moneyFormatter } = useMoney();
  // const searchConfig = {
  //   entity: 'client',
  //   displayLabels: ['name'],
  //   searchFields: 'name',
  //   outputValue: 'cliente',
  // };
  const searchConfig = {
    entity: 'client',
    displayLabels: ['name'],
    searchFields: 'client',
    // outputValue: 'client',
  };


  const deleteModalLabels = ['number'];
  const dataTableColumns = [
    {
      header: translate('Number'),
      accessorKey: 'number',
    },
    {
      header: translate('Client'),
      accessorKey: 'client.name',
    },
    {
      header: translate('Amount'),
      accessorKey: 'amount',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      Cell: ({row}) =>
        moneyFormatter({ amount: row.original.amount, currency_code: row.original.currency }),
    },
    {
      header: translate('Date'),
      accessorKey: 'date',
      Cell: ({row}) => {
        return (dayjs(row.original.date).format(dateFormat))
      },
    },
    {
      header: translate('Invoice Number'),
      accessorKey: 'invoice.number',
    },
    {
      header: translate('year'),
      accessorKey: 'invoice.year',
    },
    {
      header: translate('Payment Mode'),
      accessorKey: 'paymentMode.name'
    },
    {
      header: translate('Status'),
      accessorKey: 'status',
      Cell: ({row}) => {
        let tagStatus = tagColor(row.original.invoice.paymentStatus);
        return (
          <Tag color={tagStatus.color}>
            {row.original.invoice.paymentStatus && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
  ];

  const entity = 'payment';

  const Labels = {
    PANEL_TITLE: translate('payment'),
    DATATABLE_TITLE: translate('payment_list'),
    ADD_NEW_ENTITY: translate('add_new_payment'),
    ENTITY_NAME: translate('payment'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    disableAdd: true,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return <PaymentDataTableModule config={config} />;
}
