import dayjs from 'dayjs';
import { Tag } from 'antd';
import { tagColor } from '@/utils/statusTagColor';
import QuoteDataTableModule from '@/modules/QuoteModule/QuoteDataTableModule';
import { useMoney, useDate } from '@/settings';
import useLanguage from '@/locale/useLanguage';
import { original } from '@reduxjs/toolkit';

export default function Quote() {
  const translate = useLanguage();
  const { dateFormat } = useDate();
  const entity = 'quote';
  const { moneyFormatter } = useMoney();

  const searchConfig = {
    entity: 'client',
    displayLabels: ['name'],
    searchFields: 'name',
  };
  const deleteModalLabels = ['number', 'client.name'];
  const dataTableColumns = [
    {
      header: translate('Number'),
      accessorKey: 'number',
    },
    {
      header: translate('Client'),
      ccessorKey: 'client',
      Cell: ({row}) =>{
        return `${row.original.client.people.firstname} ${row.original.client.people.lastname}`
      }
    },
    {
      header: translate('Date'),
      accessorKey: 'date',
      Cell: ({row}) => {
        return dayjs(row.original.date).format(dateFormat);
      },
    },
    {
      header: translate('expired Date'),
      accessorKey: 'expiredDate',
      Cell: ({row}) => {
        return dayjs(row.original.expiredDate).format(dateFormat);
      },
    },
    {
      header: translate('Sub Total'),
      accessorKey: 'subTotal',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      Cell: ({row}) => {
        return moneyFormatter({ amount: row.original.total, currency_code: row.original.currency })
      }
    },
    {
      header: translate('Total'),
      accessorKey: 'total',
      onCell: () => {
        return {
          style: {
            textAlign: 'right',
            whiteSpace: 'nowrap',
            direction: 'ltr',
          },
        };
      },
      render: ({row}) => {
        return moneyFormatter({ amount: row.original.total, currency_code: row.original.currency })
      }

    },

    {
      header: translate('Status'),
      accessorKey: 'status',
      Cell: ({row}) => {
        let tagStatus = tagColor(row.original.status);

        return (
          <Tag color={tagStatus.color}>
            {tagStatus.icon + ' '}
            {row.original.status && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('quotes'),
    DATATABLE_TITLE: translate('quote_list'),
    ADD_NEW_ENTITY: translate('add_new_quote'),
    ENTITY_NAME: translate('quotes'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return <QuoteDataTableModule config={config} />;
}
