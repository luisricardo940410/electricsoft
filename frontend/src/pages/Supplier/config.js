import color from '@/utils/color';

export const fields = {
  name: {
    type: 'stringWithColor',
    required: true,
  },
  legalName: {
    type: 'string',
    required: true,
  },
  enabled: {
    type: 'boolean',
    required: true,
  },
};
