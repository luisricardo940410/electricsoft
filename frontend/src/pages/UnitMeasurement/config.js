import color from '@/utils/color';

export const fields = {
  abbreviation: {
    type: 'stringWithColor',
    required: true,
  },
  name: {
    type: 'string',
    required: true,
    disableForTable:true
  },
  description: {
    type: 'textarea',
    required: true,
    showTable:false
  },
  color: {
    type: 'color',
    options: [...color],
    required: true,
    showTable:false
  },
  enabled: {
    type: 'boolean',
    required: true,
    showTable:false
  },
};
