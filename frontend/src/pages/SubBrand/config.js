import color from '@/utils/color';
//     don't show on
export const fields = {
  name: {
    type: 'stringWithColor',
    required: true,
  },
  description: {
    type: 'textarea',
    required: true,
  },
  enabled: {
    type: 'boolean',
    required: false,
  },
};
