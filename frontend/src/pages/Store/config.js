import color from '@/utils/color';

export const fields = {
  storeCode: {
    type: 'stringWithColor',
    label:'store code',
    required: true,
    color:'gold'

    // disableForTable:true
  },
  name:{
    type: 'string',
    required: true,
  },
  description: {
    type: 'textarea',
    required: true,
  },
  manager:{
    type: 'string',
    required: true,
  },
  country:{
    type: 'country',
    color: 'green',
    required: true,
  },
  enabled: {
    type: 'boolean',
    required: true,
  },
};
