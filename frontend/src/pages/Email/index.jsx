import React from 'react';
import useLanguage from '@/locale/useLanguage';
import EmailDataTableModule from '@/modules/EmailModule/EmailDataTableModule';

export default function Email() {
  const translate = useLanguage();
  const entity = 'email';
  const searchConfig = {
    entity : 'email',
    displayLabels: ['name'],
    searchFields: 'name',
    // outputValue: '_id',
  };

  const deleteModalLabels = ['name'];

  const readColumns = [
    {
      header: translate('Template'),
      accessorKey: 'emailName',
    },
    {
      header: translate('Subject'),
      accessorKey: 'emailSubject',
    },
    {
      header: translate('email content'),
      accessorKey: 'emailBody',
    },
  ];
  const dataTableColumns = [
    {
      header: translate('Template'),
      accessorKey: 'emailName',
      // key: 'emailName',
    },
    {
      header: translate('Subject'),
      accessorKey: 'emailSubject',
      // key: 'emailSubject',
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('email_template'),
    DATATABLE_TITLE: translate('email_template_list'),
    ADD_NEW_ENTITY: translate('add_new_email_template'),
    ENTITY_NAME: translate('email_template'),
  };

  const configPage = {
    entity,
    create: false,
    ...Labels,
  };
  const config = {
    ...configPage,
    readColumns,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return <EmailDataTableModule config={config} />;
}
