import useLanguage from '@/locale/useLanguage';
import CrudModule from '@/modules/CrudModule/CrudModule';
import EmployeeForm from '@/forms/EmployeeForm';
import dayjs from 'dayjs';
import { useDate } from '@/settings';
export default function Employee() {
  const translate = useLanguage();
  const { dateFormat } = useDate();
  const entity = 'employee';
  const searchConfig = {
    displayLabels: ['name', 'surname'],
    searchFields: 'name,surname,birthday',
    outputValue: '_id',
  };

  const deleteModalLabels = ['name', 'surname'];

  const dataTableColumns = [
    {
      header: translate('first name'),
      accessorKey: 'name',
    },
    {
      header: translate('last name'),
      accessorKey: 'surname',
    },
    {
      header: translate('Birthday'),
      accessorKey: 'birthday',
      render: (date) => {
        return dayjs(date).format(dateFormat);
      },
    },
    {
      header: translate('Department'),
      accessorKey: 'department',
    },
    {
      header: translate('Position'),
      accessorKey: 'position',
    },
    {
      header: translate('Phone'),
      accessorKey: 'phone',
    },
    {
      header: translate('Email'),
      accessorKey: 'email',
    },
  ];

  const readColumns = [
    {
      header: translate('first name'),
      accessorKey: 'name',
    },
    {
      header: translate('last name'),
      accessorKey: 'surname',
    },
    {
      header: translate('Birthday'),
      accessorKey: 'birthday',
      isDate: true,
    },
    {
      header: translate('birthplace'),
      accessorKey: 'birthplace',
    },
    {
      header: translate('gender'),
      accessorKey: 'gender',
    },
    {
      header: translate('Department'),
      accessorKey: 'department',
    },
    {
      header: translate('Position'),
      accessorKey: 'position',
    },
    {
      header: translate('address'),
      accessorKey: 'address',
    },
    {
      header: translate('state'),
      accessorKey: 'state',
    },
    {
      header: translate('Phone'),
      accessorKey: 'phone',
    },
    {
      header: translate('Email'),
      accessorKey: 'email',
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('employee'),
    DATATABLE_TITLE: translate('employee_list'),
    ADD_NEW_ENTITY: translate('add_new_employee'),
    ENTITY_NAME: translate('employee'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    readColumns,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return (
    <CrudModule
      createForm={<EmployeeForm />}
      updateForm={<EmployeeForm isUpdateForm={true} />}
      config={config}
    />
  );
}
