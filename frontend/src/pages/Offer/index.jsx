import dayjs from 'dayjs';
import { Tag } from 'antd';
import { tagColor } from '@/utils/statusTagColor';

import OfferDataTableModule from '@/modules/OfferModule/OfferDataTableModule';
import { useMoney, useDate } from '@/settings';
import useLanguage from '@/locale/useLanguage';

export default function Offer() {
  const translate = useLanguage();
  const { moneyFormatter } = useMoney();
  const { dateFormat } = useDate();

  const searchConfig = {
    entity: 'lead',
    displayLabels: ['name'],
    searchFields: 'name',
  };
  const deleteModalLabels = ['number', 'lead.name'];
  const dataTableColumns = [
    {
      header: translate('Number'),
      accessorKey: 'number',
    },
    {
      header: translate('Company'),
      accessorKey: ['lead', 'name'],
    },
    {
      header: translate('Date'),
      accessorKey: 'date',
      cell: (date) => dayjs(date).format(dateFormat),
    },
    {
      header: translate('Sub Total'),
      accessorKey: 'subTotal',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      cell: (total, record) => moneyFormatter({ amount: total, currency_code: record.currency }),
    },
    {
      header: translate('Total'),
      accessorKey: 'total',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      cell: (total, record) => moneyFormatter({ amount: total, currency_code: record.currency }),
    },

    {
      header: translate('Note'),
      accessorKey: 'notes',
    },
    {
      header: translate('Status'),
      accessorKey: 'status',
      cell: (status) => {
        let tagStatus = tagColor(status);

        return (
          <Tag color={tagStatus.color}>
            {/* {tagStatus.icon + ' '} */}
            {status && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
  ];

  const entity = 'offer';
  const Labels = {
    PANEL_TITLE: translate('Offer Leads'),
    DATATABLE_TITLE: translate('offer_list'),
    ADD_NEW_ENTITY: translate('add_new_offer'),
    ENTITY_NAME: translate('Offer Leads'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return <OfferDataTableModule config={config} />;
}
