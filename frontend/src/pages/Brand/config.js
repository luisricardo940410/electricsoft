import color from '@/utils/color';
//     don't show on
export const fields = {
  name: {
    type: 'stringWithColor',
    required: true,
  },
  description: {
    type: 'textarea',
    required: true,
  },
  discount: {
    type: 'stringWithColor',
    color:'green',
    required: true,

  },
  enabled: {
    type: 'boolean',
    required: true,
  },
};
