import dayjs from 'dayjs';
import { Tag } from 'antd';
import useLanguage from '@/locale/useLanguage';
import { tagColor } from '@/utils/statusTagColor';

import { useMoney, useDate } from '@/settings';
import InvoiceDataTableModule from '@/modules/InvoiceModule/InvoiceDataTableModule';

export default function Invoice() {
  const translate = useLanguage();
  const { dateFormat } = useDate();
  const entity = 'invoice';
  const { moneyFormatter } = useMoney();

  const searchConfig = {
    entity: 'client',
    displayLabels: ['name'],
    searchFields: 'name',
  };
  const deleteModalLabels = ['number', 'client.name'];
  const dataTableColumns = [
    {
      header: translate('Number'),
      accessorKey: 'number',
    },
    {
      header: translate('Client'),
      accessorKey: 'client.name',
    },
    {
      header: translate('Date'),
      accessorKey: 'date',
      cell: ({row}) => {
        return dayjs(row.original.date).format(dateFormat);
      },
    },
    {
      header: translate('expired Date'),
      accessorKey: 'expiredDate',
      cell: ({row}) => {
        return dayjs(row.original.expiredDate).format(dateFormat);
      },
    },
    {
      header: translate('Total'),
      accessorKey: 'total',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      cell: ({row}) => {
        return moneyFormatter({ amount: row.original.total, currency_code: row.original?.currency });
      },
    },
    // {
    //   header: translate('paid'),
    //   accessorKey: 'credit',
    //   // onCell: () => {
    //   //   return {
    //   //     style: {
    //   //       textAlign: 'right',
    //   //       whiteSpace: 'nowrap',
    //   //       direction: 'ltr',
    //   //     },
    //   //   };
    //   // },
    //   cell: (total, record) => moneyFormatter({ amount: total, currency_code: record.currency }),
    // },
    {
      header: translate('Status'),
      accessorKey: 'status',
      Cell: ({row}) => {
        let tagStatus = tagColor(row.original.status);

        return (
          <Tag color={tagStatus.color}>
            {tagStatus.icon + ' '}
            {row.original.status && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
    {
      header: translate('Payment'),
      accessorKey: 'paymentStatus',
      Cell: ({row}) => {
        let tagStatus = tagColor(row.original.paymentStatus);

        return (
          <Tag color={tagStatus.color}>
            {tagStatus.icon + ' '}
            {row.original.paymentStatus && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
    // {
    //   header: translate('Created By'),
    //   accessorKey: ['createdBy', 'name'],
    // },
  ];

  const Labels = {
    PANEL_TITLE: translate('invoice'),
    DATATABLE_TITLE: translate('invoice_list'),
    ADD_NEW_ENTITY: translate('add_new_invoice'),
    ENTITY_NAME: translate('invoice'),

    RECORD_ENTITY: translate('record_payment'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };

  return <InvoiceDataTableModule config={config} />;
}
