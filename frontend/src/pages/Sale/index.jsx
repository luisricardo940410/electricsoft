import dayjs from 'dayjs';
import { Tag } from 'antd';
import { tagColor } from '@/utils/statusTagColor';
import SaleDataTableModule from '@/modules/SaleModule/SaleDataTableModule';
import { useMoney, useDate } from '@/settings';
import useLanguage from '@/locale/useLanguage';
import { useSelector } from 'react-redux';
import { selectFinanceSettings } from '@/redux/settings/selectors';
import {formatNumber} from '@/utils/helpers'

export default function Quote() {
  const translate = useLanguage();
  const { dateFormat } = useDate();
  const entity = 'sale';
  const { moneyFormatter } = useMoney();
  const { quote_prefix } = useSelector(selectFinanceSettings);

  const searchConfig = {
    entity: 'client',
    displayLabels: ['name'],
    searchFields: 'client',
  };

  const deleteModalLabels = ['number', 'client.name'];
  const dataTableColumns = [
    {
      header: translate('Number'),
      accessorKey: 'number',
      Cell: ({row}) =>{
        return `${quote_prefix} ${formatNumber(row.original.number)}`
      }
    },
    {
      header: translate('Client'),
      ccessorKey: 'client',
      Cell: ({row}) =>{
        return `${row.original.client.people.firstname} ${row.original.client.people.lastname}`
      }
    },
    {
      header: translate('Company'),
      ccessorKey: 'company',
      Cell: ({row}) =>{
        return `${row.original.company.name}`
      }
    },
    {
      header: translate('Date'),
      accessorKey: 'date',
      Cell: ({date}) => {
        return dayjs(date).format(dateFormat);
      },
    },
    {
      header: translate('expired Date'),
      accessorKey: 'expiredDate',
      Cell: ({row}) => {
        return dayjs(row.original.expiredDate).format(dateFormat);
      },
    },
    {
      header: translate('Sub Total'),
      accessorKey: 'subTotal',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      Cell: ({row}) => {
       return  moneyFormatter({ amount: row.original.subTotal, currency_code: row.original.currency })
      }

    },
    {
      header: translate('Total'),
      accessorKey: 'total',
      // onCell: () => {
      //   return {
      //     style: {
      //       textAlign: 'right',
      //       whiteSpace: 'nowrap',
      //       direction: 'ltr',
      //     },
      //   };
      // },
      Cell: ({row}) => {
        return (
          <span  style={{textAlign: 'right',whiteSpace: 'nowrap',direction: 'ltr',}}>
            {moneyFormatter({ amount: row.original.total, currency_code: row.original.currency })}
          </span>
        )
      },
    },
    {
      header: translate('Status'),
      accessorKey: 'status',
      Cell: ({row}) => {
        let tagStatus = tagColor(row.original.status);
        return (
          <Tag color={tagStatus.color}>
            {tagStatus.icon + ' '}
            {row.original.status && translate(tagStatus.label)}
          </Tag>
        );
      },
    },
  ];

  const Labels = {
    PANEL_TITLE: translate('sales'),
    DATATABLE_TITLE: translate('sales_list'),
    ADD_NEW_ENTITY: translate('add_new_sale'),
    ENTITY_NAME: translate('sales'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  const config = {
    ...configPage,
    dataTableColumns,
    searchConfig,
    deleteModalLabels,
  };
  return <SaleDataTableModule config={config} />;
}
