import useLanguage from '@/locale/useLanguage';
import ReadSaleModule from '@/modules/SaleModule/ReadSaleModule';

export default function QuoteRead() {
  const translate = useLanguage();

  const entity = 'sale';

  const Labels = {
    PANEL_TITLE: translate('sale'),
    DATATABLE_TITLE: translate('sale_list'),
    ADD_NEW_ENTITY: translate('add_new_sale'),
    ENTITY_NAME: translate('sales_counter'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  return <ReadSaleModule config={configPage} />;
}
