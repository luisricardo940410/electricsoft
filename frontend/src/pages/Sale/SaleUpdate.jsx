import useLanguage from '@/locale/useLanguage';
import UpdateSaleModule from '@/modules/SaleModule/UpdateSaleModule';

export default function QuoteUpdate() {
  const translate = useLanguage();

  const entity = 'sale';

  const Labels = {
    PANEL_TITLE: translate('sale'),
    DATATABLE_TITLE: translate('sales_list'),
    ADD_NEW_ENTITY: translate('add_new_sale'),
    ENTITY_NAME: translate('sale'),
  };

  const configPage = {
    entity,
    ...Labels,
  };
  return <UpdateSaleModule config={configPage} />;
}
