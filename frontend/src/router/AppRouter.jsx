import { lazy, useEffect } from 'react';

import {} from 'react-router-dom';
import {} from 'react-router-dom';
import { Navigate, useLocation, useRoutes } from 'react-router-dom';
import { useAppContext } from '@/context/appContext';

import routes from './routes';

export default function AppRouter() {
  let location = useLocation();
  const { state: stateApp, appContextAction } = useAppContext();
  const { app } = appContextAction;


  //TO DO:  para definir permisos mas tarde 

    // Definir las rutas permitidas
    const allowedPaths = ['/', '/home', '/dashboard', '/profile', '/settings', '/product','*' , '/logout', '/login']; // Rutas permitidas


  const routesList = [];

  Object.entries(routes).forEach(([key, value]) => {
    routesList.push(...value);


    // TO DO: para realizar la exepciones de la rutas
    // value.forEach(route => {
    //   if (allowedPaths.includes(route.path)) {
    //     routesList.push(route);
    //   }
    // });
    
  });

  function getAppNameByPath(path) {
    for (let key in routes) {
      for (let i = 0; i < routes[key].length; i++) {
        if (routes[key][i].path === path) {
          return key;
        }
      }
    }
    // Return 'default' app  if the path is not found
    return 'default';
  }
  useEffect(() => {
    if (location.pathname === '/') {
      app.default();
    } else {
      const path = getAppNameByPath(location.pathname);
      app.open(path);
    }
  }, [location]);

  let element = useRoutes(routesList);

  return element;
}
