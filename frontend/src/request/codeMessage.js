const codeMessage = {
  200: 'El servidor devolvió exitosamente los datos solicitados. ',
  201: 'Crear o modificar datos correctamente. ',
  202: 'Una solicitud ha entrado en la cola en segundo plano (tarea asincrónica). ',
  204: 'Eliminar datos correctamente. ',
  400: 'Hubo un error en la solicitud enviada y el servidor no creó ni modificó datos. ',
  401: 'El administrador no tiene permiso, intente iniciar sesión nuevamente. ',
  403: 'El administrador está autorizado, pero el acceso está prohibido. ',
  404: 'La solicitud enviada es para un registro que no existe y el servidor no está operativo. ',
  406: 'El formato solicitado no está disponible. ',
  410: 'El recurso solicitado se ha eliminado permanentemente y ya no estará disponible. ',
  422: 'Al crear un objeto, ocurrió un error de validación. ',
  500: 'Se produjo un error en el servidor, verifique el servidor. ',
  502: 'Error de puerta de enlace. ',
  503: 'El servicio no está disponible, el servidor está temporalmente sobrecargado o en mantenimiento. ',
  504: 'Se agotó el tiempo de espera de la puerta de enlace. ',
};



export default codeMessage;
