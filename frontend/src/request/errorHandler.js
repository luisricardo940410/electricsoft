import { notification } from 'antd';
import codeMessage from './codeMessage';

const errorHandler = (error) => {
  if (!navigator.onLine) {
    notification.config({
      duration: 15,
      maxCount: 1,
    });
    // Código a ejecutar cuando hay conexión a internet
    notification.error({
      message: 'Sin conexión a Internet',
      description: 'No puedo conectarme a Internet, verifique su red de Internet',
    });
    return {
      success: false,
      result: null,
      message: 'No puedo conectarme al servidor, verifique su red de Internet',
    };
  }

  const { response } = error;

  if (!response) {
    notification.config({
      duration: 20,
      maxCount: 1,
    });
    // Código para ejecutar cuando no hay conexión a internet
    notification.error({
      message: 'Problema al conectarse al servidor',
      description: 'No puedo conectarme al servidor. Vuelve a intentarlo más tarde.',
    });
    return {
      success: false,
      result: null,
      message: 'No se puede conectar al servidor. Comuníquese con el administrador de su cuenta.',
    };
  }

  if (response && response.data && response.data.jwtExpired) {
    const result = window.localStorage.getItem('auth');
    const jsonFile = window.localStorage.getItem('isLogout');
    const { isLogout } = (jsonFile && JSON.parse(jsonFile)) || false;
    window.localStorage.removeItem('auth');
    window.localStorage.removeItem('isLogout');
    if (result || isLogout) {
      window.location.href = '/logout';
    }
  }

  if (response && response.status) {
    const message = response.data && response.data.message;

    const errorText = message || codeMessage[response.status];
    const { status } = response;
    notification.config({
      duration: 20,
      maxCount: 2,
    });
    notification.error({
      message: `Error en solicitud ${status}`,
      description: errorText,
    });
    return response.data;
  } else {
    notification.config({
      duration: 15,
      maxCount: 1,
    });

    if (navigator.onLine) {
      // Code to execute when there is internet connection
      notification.error({
        message: 'Problema al conectarse al servidor',
        description: 'No puedo conectarme al servidor. Vuelve a intentarlo más tarde.',
      });
      return {
        success: false,
        result: null,
        message: 'No se puede conectar al servidor. Comuníquese con el administrador de su cuenta.',
      };
    } else {
      // Code to execute when there is no internet connection
      notification.error({
        message: 'Sin conexión a Internet',
        description: 'No puedo conectarme a Internet, verifique su red de Internet',
      });
      return {
        success: false,
        result: null,
        message: 'No puedo conectarme al servidor, verifique su red de Internet',
      };
    }
  }
};

export default errorHandler;
